<?php

use Illuminate\Database\Seeder;

class PopularTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0;$i<8;++$i) {
            $popular = new \App\Models\Popular();
            $popular->fill([
                'title' => 'Картридж hp c4129x',
                'price' => 4200,
                'slug'=>'hp'
            ]);
            $popular->save();

        }
    }
}
