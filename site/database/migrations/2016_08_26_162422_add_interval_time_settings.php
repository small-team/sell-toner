<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntervalTimeSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function ($table) {
            $table->string('time_display_phones')->default('on');
            $table->time('from_time')->nulladle();
            $table->time('before_time')->nulladle();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('time_display_phones');
            $table->dropColumn('from_time');
            $table->dropColumn('before_time');
        });
    }
}
