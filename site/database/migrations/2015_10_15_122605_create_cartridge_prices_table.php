<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartridgePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cartridge_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_brand');
            $table->integer('id_type');
            $table->string('cartridge_model');
            $table->string('articule');
            $table->float('price_RUB');
            $table->float('price_USD');
            $table->tinyInteger('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cartridge_prices');
    }
}
