<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('edb_uri');
            $table->string('satellite_key');
            $table->text('main_page_content');
            $table->text('timeblade');
            $table->string('company_phone');
            $table->text('timeblade_footer');
            $table->string('company_phone_footer');
            $table->text('copyright');
            $table->string('vk');
            $table->string('fb');
            $table->string('google');
            $table->string('twitter');
            $table->text('content_error');
            $table->text('metrika');
            $table->text('best_deals');
//
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
