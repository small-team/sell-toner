<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE settings ADD about_address TEXT');
        DB::statement('ALTER TABLE settings ADD about_contact TEXT');
        DB::statement('ALTER TABLE settings ADD about_timeblade TEXT');
        DB::statement('ALTER TABLE settings ADD about_email TEXT');
        DB::statement('ALTER TABLE settings ADD about_coordinates_l TEXT');
        DB::statement('ALTER TABLE settings ADD about_coordinates_d TEXT');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
