<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessendgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function ($table) {
            $table->string('m_viber');
            $table->string('m_whatsapp');
            $table->string('m_telegram');
            $table->string('m_skype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('m_viber');
            $table->dropColumn('m_whatsapp');
            $table->dropColumn('m_telegram');
            $table->dropColumn('m_skype');
        });
    }
}
