<?php

namespace App\Http\Controllers;

use App\Models\CartridgePrice;
use App\Models\MetaTag;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Brand;

class SearchController extends Controller
{
    public function autocomplete(Request $request) {

        $cartridges = CartridgePrice::where('active','=',1)->limit($request->get('limit'));
        if (strlen($request->get('query'))) {
            $cartridges->where('articule','like','%'.$request->get('query').'%');
        }

        $cartridges = $cartridges->get();
        if ($cartridges->count()) {
            $cartridges = $cartridges->map(function($item){
                return ['value' => $item->articule, 'label' => $item->articule,];
            });
        }

        return $cartridges;
    }

    public function search(Request $request)
    {
        MetaTag::getMetaForMenu('search');
        BreadcrumbsController::getBreadCrumbs(['title' => 'Поиск', 'href' => 'search']);

        function translit($str)
        {
            $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
            $lat = array('A', 'B', 'B', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'H', 'O', 'P', 'P', 'C', 'T', 'U', 'F', 'X', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'b', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'h', 'h', 'o', 'p', 'p', 'c', 't', 'y', 'f', 'x', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
            return str_replace($rus, $lat, $str);
        }

        $key = $request->input('key');
//        $key = str_replace('-', ' ', $key);
        $key = trim($key);
        $key = htmlspecialchars($key);
        $key = translit($key);
        $keys = explode(' ', $key);
        $is_brand = false;
        $brand = '';

        foreach ($keys as $k => $value) {
            if (!empty(Brand::where('_slug', trim($value))->first())) {
                unset($keys[$k]);
                $is_brand = true;
                $brand = Brand::where('_slug', $value)->first()->title;
            }

        }
        $keys = array_diff($keys, array(''));
        $search_result = [];
        if ($key != '') {
            foreach ($keys as $key => $value) {
                $search_result[$key] = \DB::table('cartridge_prices')
                    ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
                    ->select('cartridge_prices.id', 'cartridge_prices.articule', 'brands.title as brand', 'cartridge_prices.cartridge_model', 'cartridge_prices.price_RUB', 'cartridge_prices.price_USD')
                    ->where(function ($query) use ($value, $is_brand, $brand) {
                        $query->where('active', 1);
                        if ($is_brand) {
                            $query->where('brands.title', 'LIKE', "%$brand%");
                        }
                        $query->where('cartridge_prices.articule', 'LIKE', "%$value%");

                        $query->orWhere('cartridge_prices.cartridge_model', 'LIKE', "%$value%");
                    })->get();
            }
        }


        function makeSingleArray($arr)
        {
            if (!is_array($arr)) return false;
            $tmp = array();
            foreach ($arr as $val) {
                if (is_array($val)) {
                    $tmp = array_merge($tmp, makeSingleArray($val));
                } else {
                    $tmp[] = $val;
                }
            }
            return $tmp;
        }

        $cartridge_price_list = makeSingleArray($search_result);
        $cartridge_price_item = array();

        foreach ($cartridge_price_list as $value) {
            $cartridge_price_item[$value->articule] = $value;
        }
        $count = count($cartridge_price_list);
        return view('pages.search', ['cartridge_price_item' => $cartridge_price_item, 'key_search' => $request->input('key'), 'count' => $count]);
    }

}
