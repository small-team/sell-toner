<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\MetaTag;
use App\Models\Pages;
use App\Models\Settings;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartridgePriceController extends Controller
{

    public function getCartidgePrices($slug, $pagination = 10000)
    {
        $cartridge_brand = \DB::table('brands')->where('brands._slug',$slug)->first();
        if (!$cartridge_brand) {
            throw new NotFoundHttpException();
        }

        if (!is_int($pagination)) {
            throw new NotFoundHttpException();
        }

        MetaTag::_parseMeta(Brand::where('brands._slug',$slug)->first()->toArray());

        $info_page = Settings::first()->toArray();
        BreadcrumbsController::getBreadCrumbs(['title'=>"Прайс-листы"]);
        if(isset($cartridge_brand->title)){
            BreadcrumbsController::getBreadCrumbs(['title'=>"Скупка картриджей $cartridge_brand->title",'href'=>'price_brand']);
        }

        $cartridge_price_list=\DB::table('cartridge_prices')
            ->join('cartridge_types', 'cartridge_prices.id_type', '=', 'cartridge_types.id')
            ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
            ->select('cartridge_prices.id','cartridge_types.title as type','cartridge_prices.articule','brands.title as brand','cartridge_prices.cartridge_model','cartridge_prices.price_RUB')
            ->where('brands._slug',$slug)
            ->where('active',1);

        $md = new \Mobile_Detect();

        $is_mobile_device = $md->isMobile() || $md->isTablet();
        if($is_mobile_device){
            $cartridge_price_list =  $cartridge_price_list->paginate(100);
        }else{
            $cartridge_price_list =  $cartridge_price_list->paginate($pagination);
        }
        $cartridge_price_item   =   array();
        foreach($cartridge_price_list as $value){
            $cartridge_price_item[$value->type][]=$value;
        }
        return view('pages.cartridge_price',['cartridge_price_item'=>$cartridge_price_item,
                                            'cartridge_brand'=> $cartridge_brand,'info_page'=>$info_page,
                                            'paginate_list'=>$cartridge_price_list,
            'current_url' => route('price_brand',['slug'=>$slug]),
            'show_paging_header' => true,]);

    }

    public function downloadPriceExel($slug="all")
    {
        $filename   = "selltoner_cartridges_"."$slug"."_".date("m.d.y").".xlsx";

        $qb = \DB::table('cartridge_prices')
            ->join('cartridge_types', 'cartridge_prices.id_type', '=', 'cartridge_types.id')
            ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
            ->select('cartridge_prices.id',
                'cartridge_types.title as type',
                'cartridge_prices.articule',
                'brands.title as brand',
                'cartridge_prices.cartridge_model',
                'cartridge_prices.price_RUB',
                'cartridge_prices.price_USD')
            ->where('active', 1);
        if ( $slug != "all" ) {
            $qb->where('brands._slug', $slug);
        }
        $cartridge_price_list = $qb->get();
        $cartridge_price_item   =   array();
        foreach($cartridge_price_list as $value){
            $cartridge_price_item[$value->type][]=(array)$value;
        }

        $i = 8;
        $objPHPExcel = \PHPExcel_IOFactory::createReader('Excel2007')->load('price_new.xlsx');
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');

//        $contact_info = \DB::table("satellites")->first();
//        $objWorksheet->setCellValue("F1", $contact_info->rate);
//        $objWorksheet->setCellValue("G1", $contact_info->phone);
//        $objWorksheet->setCellValue("G2", $contact_info->email);

        foreach ($cartridge_price_item as $type => $type_items) {

            $objWorksheet->mergeCells("A$i:D$i");
            $objWorksheet->duplicateStyle($objWorksheet->getStyle("A8:D8"), "A$i:D$i");
            $objWorksheet->setCellValue("A$i", $type);

            $i++;
            foreach ($type_items as $k_trash => $item) {
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("A3"), "A$i");
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("B3"), "B$i");
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("C3"), "C$i");
                $objWorksheet->duplicateStyle($objWorksheet->getStyle("D3"), "D$i");

                $objWorksheet->setCellValue("A".$i, $item['articule']);
                $objWorksheet->setCellValue("B".$i, $item['cartridge_model']);
                $objWorksheet->setCellValue("C".$i, $item['price_USD']);
                $objWorksheet->setCellValue("D".$i, $item['price_RUB']);
                $i++;
            }
        }
// Выводим HTTP-заголовки
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=$filename" );

// Выводим содержимое файла
        $objWriter->save('php://output');


        die();
    }

}
