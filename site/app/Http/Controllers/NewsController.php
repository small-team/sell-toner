<?php

namespace App\Http\Controllers;

use App\Models\MetaTag;
use App\Models\News;
use App\Models\Pages;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use PagesController;

class NewsController extends Controller
{

    public function index()
    {
        BreadcrumbsController::getBreadCrumbs(['title'=>'Новости','href'=>'news']);
        MetaTag::getMetaForMenu('news');
        $info_page  = Pages::where('slug', '=', 'news')->first();;//  Pages::getPageInfo('news');
        $news   =   News::getNewsPaginationList(4);

        if (isset($_GET['page']) && $_GET['page'] > $news->lastPage()) {
            return redirect()->route('news');
        }

        foreach ($news as $news_item) {
            $news_item->loadFiles();
        }
        return view('pages.news',['news'=>$news ,'info_page'=> $info_page]);

    }

//
    public function indexNewsItem($_news_slug)
    {
        $news = News::where('_slug', '=', $_news_slug)
                    ->where('is_active','=',1)->firstOrFail();

        MetaTag::_parseMeta($news->toArray());

        BreadcrumbsController::getBreadCrumbs(['title'=>'Новости и статьи','href'=>'news']);
        BreadcrumbsController::getBreadCrumbs(['title'=>$news->title,'href'=>'newsitem']);

//        $news =News::getOneNews($id_news);

        $like_news  =  News::getLikeNews($news->id);

        foreach ($like_news as $news_item) {
            $news_item->loadFiles();
        }

        return view('pages.newsitem',['news'=>$news,'like_news'=>$like_news]);
    }
}
