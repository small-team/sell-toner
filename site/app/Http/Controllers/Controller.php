<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\BrandPicture;
use App\Models\CartridgePrice;
use App\Models\Menu;
use App\Models\MetaTag;
use App\Models\News;
use App\Library\Cart;
use App\Models\Settings;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {    


        BreadcrumbsController::getBreadCrumbs(['title' => 'Главная', 'href' => 'home']);

        MetaTag::getMetaForMenu('home');

        $menu_item = \DB::table('menus')
            ->select('pages.slug as user_slug',
                    'meta_tags.slug as fixed_slug',
                    'object_type',
                    'show_head',
                    'show_footer',
                    '_position',
                    'menus.title')
            ->leftJoin('pages', 'menus.object_id', '=', 'pages.id')
            ->leftJoin('meta_tags', 'menus.object_id', '=', 'meta_tags.id')
            ->orderBy('_position')
            ->get();
        $menu_footer = array_chunk($menu_item, count($menu_item) / 2 + 1);

        \View::share(['menu_item' => $menu_item, 'menu_footer' => $menu_footer]);

        //cartridge brands and count
        $info_brands = CartridgePrice::select(\DB::raw('count(id_brand) as brand_count, title, brands._slug'))
            ->rightJoin('brands', 'id_brand', '=', 'brands.id')
            ->having('brand_count', '>', 0)
            ->groupBy('brands.id')
            ->orderBy('_position')
            ->get();
        \View::share('brands', $info_brands);
        
        //info_company
        $info_company = Settings::first();
        \View::share('info_company', $info_company);

        //news
        $new_news = News::getNewNews();
        \View::share('new_news', $new_news);

        Cart::putInfoToView();
        //info_footer
        $info_footer = \DB::table('settings')->first();
        \View::share('info_footer', $info_footer);

        $brands_picture = BrandPicture::all();
        \View::share('brands_picture', $brands_picture);


    }
}
