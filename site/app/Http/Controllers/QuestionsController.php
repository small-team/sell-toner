<?php

namespace App\Http\Controllers;

use App\Models\MetaTag;
use App\Models\Questions;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use  App\library\RealIp;

class QuestionsController extends Controller
{

    public function index()
    {
        BreadcrumbsController::getBreadCrumbs(['title'=>'Задать вопрос','href'=>'questions']);
        MetaTag::getMetaForMenu('questions');
        $questions = \DB::table('questions')->paginate(10);
        $info_page = \DB::table('pages')->where('slug','questions')->first();

        return view('pages.question',['questions'=> $questions,'info_page'=>$info_page]);
    }

    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'code' => 'required|captcha'
        ]);

        if ($v->fails())
        {
            return redirect()->back()
                ->withErrors($v)
                ->withInput();
        }
        else {

            $question = new Questions();
            $question->name = $request->input('name');
            $question->city = $request->input('city');
            $question->question = $request->input('mess');
            $question->IP_user = RealIp::get_client_ip_server();
            $question->save();

            $date_time_to_email =   date("d.m.y H:i:s");
            $addres_email_message   =   \DB::table('satellites')->select('orders_email')->first()->orders_email;
            $adresses = explode(';',$addres_email_message);
            $_address_site  =   \DB::table('satellites')->select('url')->first()->url;
            if($_address_site[strlen($_address_site)-1] == "/"){
                $_address_site = substr($_address_site,0,-1);
            }
            $data_mail=['date'=>$date_time_to_email,
                'name'=>$request->input('name'),
                'city'=>$request->input('place'),
                'question'=>$request->input('mess'),
                'site_url'=>$_address_site,
                'type'=>'Вопрос клиента'];
            foreach ($adresses as $_address) {
//
                \Mail::send('emails.request', $data_mail, function ($message) use ($_address,$data_mail) {
//                    $message->from($_address, 'Tonersell');
                    $message->replyTo($_address, $data_mail['name']);
                    $message->to($_address)->subject("Вопрос клиента");
                });
            }



            return redirect('questions')->with('send', true);
        }
        return redirect('questions');
    }


}
