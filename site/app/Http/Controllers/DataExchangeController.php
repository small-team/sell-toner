<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\CartridgeOrder;
use App\Models\CartridgePrice;
use App\Models\CartridgeType;
use App\Models\Settings;
use Curl\Curl;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DataExchangeController extends Controller
{

    public function dataExchange(){
        if(Settings::dataExchange()){
            return redirect()->back()->with(['send'=>true]);
        }

    }

    public function LoadOrders(Request $request)
    {
        $key           =   $request->input('key');
        $key = trim($key);

        $settings   =   Settings::first()->toArray();

        \Log::useFiles(storage_path().'/logs/exchange.log');
        \Log::info($key);
        \Log::info($settings['satellite_key']);
        \Log::info($settings['satellite_key'] !== $key ? 0 : 1);

        if (!$key) {
            dd(1,$key);
            die();
        }

        if ($settings['satellite_key'] !== $key) {
            dd(2,$key,$settings);
            die();
        }

        $orders_not_send =   \DB::table('cartridge_orders')->where('sended',0)->get();

        \Log::info(print_r($orders_not_send,true));

//        $orders_not_send    =   CartridgeOrder::where('sended',0)->get()->toArray();
        $orders   =   [];
        foreach($orders_not_send as $val){
            $val=(array)$val;
            $orders[$val['id']]=$val;
        }

        if (!is_array($orders) || empty($orders)) {
            dd(3,$key,$settings);
            die();
        }

        \Log::info(print_r($orders,true));

        $res = array();
        $order_info = array();
        $order_photo = array();
        foreach ($orders as $order) {
            $id = isset($order['id']) ? $order['id'] : false;
            $res[$id]            = $order;
            $order_info[$id]     =   \DB::table('orders_info')->where('id_order',$id)->get();
            $order_photo[$id]    = \DB::table('orders_photo')->where('id_order',$id)->get();
            \DB::table('cartridge_orders')->where('id', $id)->update(['sended' => 1]);
        }

        \Log::info(print_r($res,true));
        \Log::info(print_r($order_info,true));
        \Log::info(print_r($order_photo,true));

//        dd(array('orders' => $res, 'orders_info' => $order_info,'orders_photo' => $order_photo));

        echo json_encode(array('orders' => $res, 'orders_info' => $order_info,'orders_photo' => $order_photo));
        die();
    }
}
