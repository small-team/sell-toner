<?php

namespace App\Http\Controllers;

use App\Models\CartridgeOrder;
use App\Models\MetaTag;
use App\Models\Satellite;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use SmartModel\Exceptions\ValidationException;

class RequestController extends Controller
{
    public function index()
    {
        BreadcrumbsController::getBreadCrumbs(['title'=>'Оставить заявку','href'=>'request']);
        MetaTag::getMetaForMenu('request');
        $info_page = \DB::table('pages')->where('slug', 'request')->first();
        return view('pages.request', ['info_page' => $info_page]);
    }

    public function store_request(Request $request)
    {
        
        if($request->input('type') != CartridgeOrder::TYPE_QUICK_REQUEST && $request->input('type') != CartridgeOrder::TYPE_RETAIN_REQUEST){
            return false;
        }

        $response = (new \ReCaptcha\ReCaptcha(env('reCAPTCHA_SECRET')))
//            ->setExpectedAction('store_request')
            ->verify($request->input('g-recaptcha-response'), $request->ip());

        if (!$response->isSuccess()) {
//            dd($response->getErrorCodes());
            return redirect()->back()
                ->withInput();
        }

        $rules = [
            'hcptch' => 'required|hiddencaptcha:5,3600',
        ];
        $validator = \Validator::make(['hcptch' => $request->input('hcptch')], $rules);
        if ($validator->fails())
        {
            var_dump($validator->errors()); die();
            throw new ValidationException($validator->errors());
            return false;
        }

        $id_satellite   =   Satellite::first()->toArray()['id_satellite'];

        $data   =   [
                    'fio'=>$request->input('name'),
                    'phone'=>$request->input('phone'),
                    'email'=>$request->input('email'),
                    'city'=>$request->input('place'),
                    'comment'=>$request->input('mess'),
                    'type'=> $request->input('type'),
                    'id_satellite'=>$id_satellite,
        ];

        $insertedId =   CartridgeOrder::saveRequest($data);

        $code   =   $insertedId."100".$data['id_satellite'];
        CartridgeOrder::where('id', $insertedId)->update(['code' =>$code]);
////save image
        $images =   \Input::file('images');
        $link_image_for_orders=[];
        if(!empty($images[0]))
        {
            $i=1;
            $link_image_for_orders=[];
            foreach($images as $image)
            {
                $imageName = "order_".$code."_".$i.".".$image->getClientOriginalExtension();
                $i++;
                $image->move(base_path() . "/public/upload/photo-zayavki/"."$code", $imageName);
                $image_link = "/upload/photo-zayavki/"."$code"."/".$imageName;
                $link_image_for_orders[]=['id_order'=>$insertedId,
                                    'link'=> $image_link];
            }
            \DB::table('orders_photo')->insert($link_image_for_orders);
        }
        $type_request   =   '';
            $date_time_to_email =   date("d.m.y H:i:s");
            $addres_email_message   =   \DB::table('satellites')->select('orders_email')->first()->orders_email;
            $addresses = explode(';',$addres_email_message);
            $data_mail  =[];
            $_address_site  =   \DB::table('satellites')->select('url')->first()->url;
            if($_address_site[strlen($_address_site)-1] == "/"){
                $_address_site = substr($_address_site,0,-1);
            }
            foreach($link_image_for_orders as $key=>$val){
                $link_image_for_orders[$key]['link']= $_address_site.$val['link'];
            }
            if($request->input('type') == 'quick_request'){
                $type_request   =   'Быстрая заявка c главной';
            }elseif($request->input('type') == 'retain_request'){
                $type_request   =   'Заявка с меню Оставить заявку';
            }
            $data_mail=['code'=>$code,
                'date'=>$date_time_to_email,
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'city'=>$request->input('place'),
                'email'=>$request->input('email'),
                'comment'=>$request->input('mess'),
                'site_url'=>$_address_site,
                'type'=>$type_request,
                'photo_links'=>$link_image_for_orders];

        $from_email =   $request->input('email');

        if ($from_email && strpos($from_email, '@') !== false) {
            \Mail::send('emails.to_user_order', $data_mail, function ($message) use ($from_email,$data_mail,$type_request) {
                $message->from('order@sell-toner.ru', 'Sell-toner');
                $message->to($from_email)->subject($type_request);
            });
        }
        
        self::sendMail($addresses,$data_mail,$code,$type_request,$data);


        return redirect()->back()->with(['send'=>true,'code'=>$code,'date'=>date('d.m.Y')]);
    }

    public function call_back(Request $request)
    {
        if($request->input('type') != CartridgeOrder::TYPE_CALL_BACK ){
            return false;
        }

        $response = (new \ReCaptcha\ReCaptcha(env('reCAPTCHA_SECRET')))
//            ->setExpectedAction('store_request')
            ->verify($request->input('g-recaptcha-response'), $request->ip());

        if (!$response->isSuccess()) {
            return ['errors' => 'recaptcha'];
        }

        $v = \Validator::make($request->all(),
            [
                'phone' =>'required',
            ]
        );

        if ($v->fails()) {
            return [
                'errors' => $v->errors()->toArray()
            ];
        }

        $orders = new CartridgeOrder();

        $orders->fio = $request->input('name');
        $orders->phone = $request->input('phone');
        $orders->type = $request->input('type');
        $orders->comment = $request->input('mess');
        $orders->id_satellite=Satellite::first()->toArray()['id_satellite'];

        $orders->save();

        $insertedId = $orders->id;
        $id_satellite = \DB::table('satellites')
            ->select('id_satellite')
            ->first();
        $id_satellite = $id_satellite->id_satellite;
        $code   =   $insertedId.'100'.$id_satellite;
        \DB::table('cartridge_orders')
            ->where('id', $insertedId)
            ->update(['code' =>$code]);
        try {
            $date_time_to_email =   date("d.m.y H:i:s");
            $addres_email_message   =   \DB::table('satellites')->select('orders_email')->first()->orders_email;
            $adresses = explode(';',$addres_email_message);
            $_address_site  =   \DB::table('satellites')->select('url')->first()->url;
            if($_address_site[strlen($_address_site)-1] == "/"){
                $_address_site = substr($_address_site,0,-1);
            }
            $data_mail=['code'=>$code,
                'date'=>$date_time_to_email,
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'site_url'=>$_address_site,
                'type'=>'Заявка на обратный звонок',];
            foreach ($adresses as $_address) {
//
                \Mail::send('emails.request', $data_mail, function ($message) use ($_address,$code, $data_mail) {
//                    $message->from($_address, 'Tonersell');
//                    $message->replyTo($_address, $data_mail['name']);
                    $message->to($_address)->subject("Заявка на обратный звонок № " .$code);
                });
            }
            
        } catch (\Exception $e) {
            \Log::info($e->getMessage().'Заявка на обратный звонок');
        }

        return array('send'=>true);
//        return redirect()->back();
    }

    public function sendMail($addresses,$data_mail,$code,$type_request,$data){
        \Log::useDailyFiles(storage_path().'/logs/send_mail.log');
        try {
            foreach ($addresses as $_address) {

                if(\Mail::send('emails.request', $data_mail, function($message) use ($_address,$code,$type_request,$data)
                {
                    $message->from('order@sell-toner.ru', 'Selltoner');
                    $message->to($_address)->subject($type_request." № " .$code);
                } )
                ){
                    \Log::info($type_request.' Letter sent to '. $_address);
                }else{
                    \Log::info($type_request.' The letter was not sent');
                }
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage().$type_request);
        }
    }
}
