<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\CartridgeOrder;
use App\Models\CartridgePrice;
use App\Models\CartridgeType;
use App\Models\MetaTag;
use App\Models\News;
use App\Models\Pages;
use App\Models\Popular;
use App\Models\Settings;
use App\Models\Slider;
use Curl\Curl;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StaticPagesController extends Controller
{

    public function indexHome()
    {
        $populars = Popular::all();
        $news = News::all()->sortByDesc('created_at')->take(2);

        return view('pages.home',compact('populars','news'));
    }

    public function indexContacts()
    {
        BreadcrumbsController::getBreadCrumbs(['title' => 'Контакты', 'href' => 'about']);
        MetaTag::getMetaForMenu('contacts');

        $about_info = Settings::first()->toArray();

        return view('pages.contacts', ['about_info' => $about_info]);
    }

    public function indexUserPages($slug)
    {
        $info_page = Pages::getPageInfo($slug);

        BreadcrumbsController::getBreadCrumbs(['title' => $info_page->title, 'href' => $info_page->slug]);
        MetaTag::getMetaUserMenu($slug);

        return view('pages.user_pages', ['info_page' => $info_page]);
    }
}
