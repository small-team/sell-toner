<?php

namespace App\Http\Controllers;

use App\Models\CartridgeOrder;
use App\Models\MetaTag;
use App\Models\Satellite;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use SmartModel\Exceptions\ValidationException;
use Symfony\Component\HttpKernel\Tests\DataCollector\DumpDataCollectorTest;
use App\Library\Cart;

class CartController extends Controller
{
    public function index()
    {
        MetaTag::getMetaForMenu('cart');
        BreadcrumbsController::getBreadCrumbs(['title'=>'Корзина','href'=>'cart']);

        if(!empty(\Session::get('order'))){
            $cart_item_all  =   \Session::get('order');
            $cart_items =[];
            foreach ($cart_item_all as $key=>$val) {
                if($val['checked']){
                    $cart_items[$key]=$val;
                }
            }
            $cart_item_id = array_keys($cart_items);
            $cart_orders = \DB::table('cartridge_prices')
                ->join('cartridge_types', 'cartridge_prices.id_type', '=', 'cartridge_types.id')
                ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
                ->select('cartridge_types.id as typeId',
                        'cartridge_prices.id',
                        'cartridge_types.title as type',
                        'cartridge_prices.articule',
                        'brands.title as brand',
                        'cartridge_prices.cartridge_model',
                        'cartridge_prices.price_RUB')
                ->whereIn('cartridge_prices.id', $cart_item_id)->get();

            $cart_orders_item   =   array();
            foreach($cart_orders as $value){
                $cart_orders_item[$value->type][]=$value;
            }
            return view('pages.cart',['cart_orders_item'=>$cart_orders_item]);

        }
        return view('pages.cart');
    }

    public function cart_update(Request $request)
    {
        $checked = true;
        if(!$request->input('checked')){
            $checked = false;
        }
//        receive data from the request
        $id_order   =   $request->input('id');
        $count  =    $request->input('count');
        $price  =   $request->input('price_RUB');
//        write to the session
        \Session::put("order.$id_order.count", $count);
        \Session::put("order.$id_order.sum", $price);
        \Session::put("order.$id_order.checked", $checked);

        return CartController::recalculation();
    }

    public function cart_item_delete(Request $request)
    {
        $id_order   =   $request->input('id');
        \Session::forget("order.$id_order");

        return CartController::recalculation();
    }

    public function clean_cart()
    {
        if(!empty(\Session::get('order'))) {
            \Session::forget('order');
        }
        return redirect()->back();
    }

    public function save_order(Request $request){
        BreadcrumbsController::getBreadCrumbs(['title'=>'Корзина','href'=>'cart']);

            if(!empty(\Session::get('order'))) {

                if($request->input('type') != CartridgeOrder::TYPE_CART_REQUEST ){
                    return false;
                }

                $response = (new \ReCaptcha\ReCaptcha(env('reCAPTCHA_SECRET')))
                    ->verify($request->input('g-recaptcha-response'), $request->ip());

                if (!$response->isSuccess()) {
//            dd($response->getErrorCodes());
                    return redirect()->back()
                        ->withInput();
                }

                $rules = [
                    'hcptch' => 'required|hiddencaptcha:5,3600',
                ];
                $validator = \Validator::make(['hcptch' => $request->input('hcptch')], $rules);
                if ($validator->fails())
                {
                    throw new ValidationException($validator->errors());
                    return false;
                }


                $orders = new CartridgeOrder();

                $cart_items = \Session::get('order');
                $count_items = 0;
                $sum_items = 0;
                foreach ($cart_items as $value) {
                    $count_items += $value["count"];
                    $sum_items += $value["count"] * $value["sum"];

                }
                $id_satellite   =   Satellite::first()->toArray()['id_satellite'];
                $orders->type = $request->input('type');
                $orders->comment = $request->input('mess');
                $orders->sum = $sum_items;
                $orders->quantity = $count_items;
                $orders->fio = $request->input('name');
                $orders->city = $request->input('place');
                $orders->phone = $request->input('phone');
                $orders->email = $request->input('email');
                $orders->id_satellite = $id_satellite;


                $orders->save();
                $insertedId = $orders->id;
                $id_satellite = \DB::table('satellites')
                    ->select('id_satellite')
                    ->first();
                $id_satellite = $id_satellite->id_satellite;
                $code   =   $insertedId.'100'.$id_satellite;
                \DB::table('cartridge_orders')
                    ->where('id', $insertedId)
                    ->update(['code' =>$code]);

                $cart_items_ids = array_keys($cart_items);
                $carttridge_prices_item=\DB::table('cartridge_prices')
                    ->join('brands', 'cartridge_prices.id_brand', '=', 'brands.id')
                    ->select('cartridge_prices.id as cp_id','cartridge_model','articule','price_RUB','brands.title')
                    ->whereIn('cartridge_prices.id', $cart_items_ids)->get();

                foreach($carttridge_prices_item as $value)
                {
                    $data[] = [ 'id_order'=> $insertedId,
                        'brand'   => $value->title,
                        'model'   => $value->cartridge_model,
                        'quantity'=> $cart_items[$value->cp_id]['count'],
                        'price'   => $cart_items[$value->cp_id]['sum'],
                        'articule'=> $value->articule,
                        'sum'     => $cart_items[$value->cp_id]['count']*$cart_items[$value->cp_id]['sum']
                    ];
                }
                \DB::table('orders_info')->insert($data);

//save image
                $images =   \Input::file('images');
                $link_image_for_orders=[];
                if(!empty($images[0]))
                {
                    $i=1;
                    $link_image_for_orders=[];
                    foreach($images as $image)
                    {
                        $imageName = "order_".$insertedId."_".$i.".".$image->getClientOriginalExtension();
                        $i++;
                        $image->move(base_path() . "/public/upload/photo-zayavki/"."$insertedId", $imageName);
                        $image_link = "/upload/photo-zayavki/"."$insertedId"."/".$imageName;
                        $link_image_for_orders[]=['id_order'=>$insertedId,
                                                  'link'    => $image_link];
                    }
                    \DB::table('orders_photo')->insert($link_image_for_orders);
                }
                try {
                    $date_time_to_email =   date("d.m.y H:i:s");
                    $addres_email_message   =   \DB::table('satellites')->select('orders_email')->first()->orders_email;
                    $adresses = explode(';',$addres_email_message);
                    $_address_site  =   \DB::table('satellites')->select('url')->first()->url;
                    if($_address_site[strlen($_address_site)-1] == "/"){
                        $_address_site = substr($_address_site,0,-1);
                    }
                    foreach($link_image_for_orders as $key=>$val){
                        $link_image_for_orders[$key]['link']= $_address_site.$val['link'];
                    }
                    $data_mail=['code'=>$code,
                        'date'=>$date_time_to_email,
                        'name'=>$request->input('name'),
                        'phone'=>$request->input('phone'),
                        'city'=>$request->input('place'),
                        'email'=>$request->input('email'),
                        'comment'=>$request->input('mess'),
                        'site_url'=>$_address_site,
                        'type'=>'Заявка с корзины',
                        'order_info'=>$data,
                        'sum_items'=>$sum_items,
                        'count_items'=>$count_items,
                        'photo_links'=>$link_image_for_orders];
                    $from_email = $request->input('email');


//
//                    dd($data);
                    $filename   = "ts".$insertedId.".xlsx";
                    $i = 11;
                    $objPHPExcel = \PHPExcel_IOFactory::createReader('Excel2007')->load('test.xlsx');
                    $objWorksheet = $objPHPExcel->getActiveSheet();
                    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');

                    $objWorksheet->mergeCells("A2:E2");
                    $objWorksheet->duplicateStyle($objWorksheet->getStyle("G1"), "A2");
                    $objWorksheet->setCellValue("A2", "Заявка № ".$insertedId.'100'.$id_satellite." от ".$date_time_to_email);
                    $objWorksheet->setCellValue("B4", $data_mail['name']);
                    $objWorksheet->setCellValue("B5", $data_mail['city']);
                    $objWorksheet->setCellValue("B6", $data_mail['phone']);
                    $objWorksheet->setCellValue("B7", $data_mail['email']);
                    $objWorksheet->setCellValue("B8", $data_mail['comment']);

                    foreach ($data as $items => $info_items) {
                            $objWorksheet->duplicateStyle($objWorksheet->getStyle("A10"), "A$i");
                            $objWorksheet->duplicateStyle($objWorksheet->getStyle("B10"), "B$i");
                            $objWorksheet->duplicateStyle($objWorksheet->getStyle("C10"), "C$i");
                            $objWorksheet->duplicateStyle($objWorksheet->getStyle("D10"), "D$i");
                            $objWorksheet->duplicateStyle($objWorksheet->getStyle("E10"), "E$i");

                            $objWorksheet->setCellValue("A".$i, $info_items['articule']);
                            $objWorksheet->setCellValue("B".$i, $info_items['model']);
                            $objWorksheet->setCellValue("C".$i, $info_items['quantity']);
                            $objWorksheet->setCellValue("D".$i, $info_items['price']);
                            $objWorksheet->setCellValue("E".$i, $info_items['sum']);
                            $i++;
                    }
                    $objWorksheet->duplicateStyle($objWorksheet->getStyle("G1"), "A".$i);
                    $objWorksheet->duplicateStyle($objWorksheet->getStyle("G1"), "B".$i);
                    $objWorksheet->setCellValue("A".$i,'Итого:');
                    $objWorksheet->setCellValue("B".$i, $sum_items. " руб.");

                    $objWriter->save(base_path() . "/public/".$filename);
                    \Log::useDailyFiles(storage_path().'/logs/send_mail.log');



                    $email_address = $request->input('email');
                    \Mail::send('emails.to_user_order', $data_mail, function ($message) use ($email_address, $data_mail) {
                        $message->from('order@sell-toner.ru', 'Sell-toner');
                        $message->to($email_address)->subject('Заявка с корзины');
                    });
                    
                    if (file_exists( base_path() . "/public/".$filename )) {
                        $from_email =  'info@sell-toner.ru';
                        foreach ($adresses as $_address) {
                            if( \Mail::send('emails.cart_request', $data_mail, function($message) use ($_address,$code,$from_email,$filename,$data_mail)
                            {
                                $message->from('order@sell-toner.ru', 'Selltoner');
                                $message->to($_address)->subject("Заявка с корзины № " .$code);
                                $message->attach(base_path()."/public/".$filename);
                            }))
                            {
                                \Log::info('From Cart letter sent to '. $_address);
                            }else{
                                \Log::info('From Cart letter was not sent '. $_address);
                            }
                        }
                    }


                \File::delete($filename);
                } catch (\Exception $e) {
                    \Log::info($e->getMessage().'Заявка с корзины');
                }


                \Session::forget('order');

                Cart::putInfoToView();

                return view('pages.cart',['send'=>true,'code'=>$insertedId.'100'.$id_satellite,'date'=>date('d.m.Y')]);
            }
            return view('pages.cart',['send'=>false]);



    }

    public function recalculation(){
        $cart_items = \Session::get('order');
        $count_items = 0;
        $sum_items = 0;
        foreach($cart_items as $value)
        {
            if($value["checked"]!=0)
            {
                $count_items+=$value["count"];
                $sum_items+=$value["count"]*$value["sum"];
            }
        }
        return array('count'=>$count_items,'sum'=>$sum_items);
    }


}
