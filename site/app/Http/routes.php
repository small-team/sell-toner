<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//home
get('/',['as'=>'home','uses'=>'StaticPagesController@indexHome']);
//user pages
get('/pages/{slug}',['as'=>'user_pages','uses'=>'StaticPagesController@indexUserPages']);
//about
get('/about',['as'=>'about','uses'=>'StaticPagesController@indexAbout']);
//working conditions
get('/rules',['as'=>'rules','uses'=>'StaticPagesController@indexRules']);
//contacts
//get('/contacts',['as'=>'contacts','uses'=>'StaticPagesController@indexContacts']);
//regions
get('/regions',['as'=>'regions','uses'=>'StaticPagesController@indexRegions']);

get('/data_exchange',['as'=>'data_exchange','uses'=>'DataExchangeController@dataExchange']);
//dataExchange
get('/load_orders',['as'=>'data_exchange','uses'=>'DataExchangeController@LoadOrders']);



//GetCartridgePrice
get('/price/{slug}/{pagination?}',['as'=>'price_brand','uses'=>'CartridgePriceController@getCartidgePrices']);
//DownloadCartridgePrice
get('/exel/{slug?}',['as'=>'exel_download','uses'=>'CartridgePriceController@downloadPriceExel']);



//request
//get('/request',['as'=>'request','uses'=>'RequestController@index']);
//store request
post('/store_request',['as'=>'store_request','uses'=>'RequestController@store_request']);
//store request call back
post('/call_back',['as'=>'call_back','uses'=>'RequestController@call_back']);


//news
get('/news',['as'=>'news','uses'=>'NewsController@index']);
//newsitem
get('/news/{_news_slug}',['as'=>'newsitem','uses'=>'NewsController@indexNewsItem']);

//


//questions
//get('/questions',['as'=>'questions','uses'=>'QuestionsController@index']);
//store questions
//get('/questions_store',['as'=>'store_questions','uses'=>'QuestionsController@store']);

//cart
get('/cart',['as'=>'cart','uses'=>'CartController@index']);
//cart update
post('/cart_update',['as'=>'cart_add','uses'=>'CartController@cart_update']);
//cart order delete
post('/cart_item_delete',['as'=>'cart_item_delete','uses'=>'CartController@cart_item_delete']);
//clean session orders
get('/clean_cart',['as'=>'clean_cart','uses'=>'CartController@clean_cart']);
//add to orders
post('/cart_item_delete',['as'=>'cart_item_delete','uses'=>'CartController@cart_item_delete']);
//save order
post('/save_order',['as'=>'save_order','uses'=>'CartController@save_order']);


//search
get('/search',['as'=>'search','uses'=>'SearchController@search']);
get('/autocomplete',['as'=>'autocomplete','uses'=>'SearchController@autocomplete']);

Route::group(['prefix'=>'uploader','nocsrf' => true],function(){
    Route::match(['get','post'],'/','Fileuploader@upload');
});