<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Popular extends SmartModel
{
    public static $files = array(
        'image' => array(
            'sizes' => array(
                'list' => array(
                    'size' => '154x99',
                    'process' => 'fitOut',
                ),
            ),
        )
    );

    public function getImageUrlAttribute()
    {
        $this->loadFiles();
        if (count($this->image['sizes']['list']['link'])){
            return asset($this->image['sizes']['list']['link']);
        }
        return asset('img/products/02.png');
    }

}
