<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Pages extends SmartModel
{
    public static function getPageInfo($_slug){
        return Pages::where('slug',$_slug)->firstOrFail();
    }
}
