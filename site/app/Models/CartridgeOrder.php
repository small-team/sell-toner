<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class CartridgeOrder extends SmartModel
{
    public $table = 'cartridge_orders';
    protected $appends = ['orders_product_info','order_photos_links',];

    //'Быстрая заявка с главной'
    const TYPE_QUICK_REQUEST = 'quick_request';
//    'Заявка с меню Оставить заявку',
    const TYPE_RETAIN_REQUEST = 'retain_request';
//    'Заявка на обратный звонок'
    const TYPE_CALL_BACK = 'call_back';
//    'Заявка с корзины'
    const TYPE_CART_REQUEST = 'cart_request';

    public function getOrdersProductInfoAttribute(){
       return( \DB::table('orders_info')
           ->where('id_order',$this->id)->get());
    }

    public function getOrderPhotosLinksAttribute(){
       $photos = \DB::table('orders_photo')
           ->where('id_order',$this->id)->get();
        return $photos;
    }

    public static function saveRequest($data){
        $orders = new CartridgeOrder();
        foreach($data as $key => $value){
            $orders->$key   =    $value;
        }
        $orders->save($data);
        $insertedId = $orders->id;
        return $insertedId;
    }
}
