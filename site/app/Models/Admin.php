<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Admin extends SmartModel
{
    public  $timestamps = false;
    public $table   =   'admins';



    /**
     * The database table used by the model.
     *
     * @var string
     */

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password',);

    public static function login($login, $password) {
        $login = trim(strip_tags($login));
        $password = sha1($password);
        \Log::info($login.$password);

        $adminModel = new Admin;
        $admin_user = $adminModel->where('login',  $login)->where('password',$password)->first();
        if(is_null($admin_user) || empty($admin_user)) {
            return false;
        }
        \Session::set('admin-info', $admin_user->toArray());
        return $admin_user;
    }

    public static function logout() {
        \Session::forget('admin-info');
    }

    public static function isLoggedIn() {
        $info = \Session::get('admin-info');
        $res = is_array($info) && !empty($info);
        return $res;
    }

    public static function isAdminViewMode() {
        $view_mode = \Input::get('view_mode',false);
        return self::isLoggedIn() && ($view_mode == 'admin');
    }
}
