<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class MetaTag extends SmartModel
{
    protected $table = 'meta_tags';
    public $timestamps = false;
//обязательные поля
    public $rules = array(
        'title' => 'required',
        'meta_title' => 'required',
    );


    protected $fillable = [];
//загружаемые файлы
//    public static $files = array(
//        'og_image' => array(
//            'name' => 'native',
//        ),
// );

//значения
        private static $fields = array(
            'meta_title' => 'title',
            'meta_keywords' => 'keywords',
            'meta_description' => 'description',
            'og_title' => 'og_title',
            'og_description' => 'og_description',
            );


    private static $meta_data = [];
//для конкретной страницы
    public static function _parseMeta($data){
        foreach ($data as $field=>$value) {
//            dd($data);
            if (array_key_exists($field,self::$fields)) {
//                dd(1);

                self::$meta_data[self::$fields[$field]] = $value;
            }
        }
    }
//для общых страниц те что есть в меню
    public static function getMetaForMenu($active_menu) {
        $meta = MetaTag::where('slug', '=', $active_menu)->first();
//        dd($meta);
        if (!$meta) {
            return false;
        }

        $meta_section = $meta->loadFiles()->toArray();
//        dd($meta_section);
        self::_parseMeta($meta_section);
    }

    public static function getMetaUserMenu($active_menu) {
        $meta = Pages::where('slug', '=', $active_menu)->first();
//        dd($meta);
        if (!$meta) {
            return false;
        }

        $meta_section = $meta->loadFiles()->toArray();
//        dd($meta_section);
        self::_parseMeta($meta_section);
    }

    public static function printMetaData() {
        $html = '';

        if (isset(self::$meta_data['title']) && !empty(self::$meta_data['title'])) {
            $html .= '<title>'.self::$meta_data['title'].'</title>'.PHP_EOL;

            if (!isset(self::$meta_data['og_title']) || empty(self::$meta_data['og_title'])) {
                self::$meta_data['og_title'] = self::$meta_data['title'];
            }
        }
        if (isset(self::$meta_data['description']) && !empty(self::$meta_data['description'])) {
            $html .= '<meta name="description" content="'.self::$meta_data['description'].'">'.PHP_EOL;
        }
        if (isset(self::$meta_data['keywords']) && !empty(self::$meta_data['keywords'])) {
            $html .= '<meta name="keywords" content="'.self::$meta_data['keywords'].'">'.PHP_EOL;
        }
        if (isset(self::$meta_data['og_title']) && !empty(self::$meta_data['og_title'])) {
            $html .= '<meta property="og:title" content="'.self::$meta_data['og_title'].'">'.PHP_EOL;
        }
        if (isset(self::$meta_data['og_description']) && !empty(self::$meta_data['og_description'])) {
            $html .= '<meta property="og:description" content="'.self::$meta_data['og_description'].'">'.PHP_EOL;
        }
        if (isset(self::$meta_data['og_image']) && !empty(self::$meta_data['og_image'])) {
            $html .= '<meta property="og:image" content="'.\URL::to('/').self::$meta_data['og_image']['link'].'">'.PHP_EOL;
        }
        echo $html;
    }
}
