<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Menu extends SmartModel
{
    protected $appends = ['id_user_pages','id_fixed_pages'];

    public  function getIdUserPagesAttribute(){
        return $this->object_id;
    }

    public  function getIdFixedPagesAttribute(){
        return $this->object_id;
    }

    public  function setIdUserPagesAttribute($val)
    {

        if($this->object_type == 'user_pages'){
            $this->object_id   =   $val;
        }
    }
    public  function setIdFixedPagesAttribute($val)
    {
        if($this->object_type == 'fixed_pages'){
            $this->object_id   =   $val;
        }
    }


}
