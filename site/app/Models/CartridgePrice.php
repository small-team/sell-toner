<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class CartridgePrice extends SmartModel
{
    protected $fillable = ['id','id_brand','id_type','cartridge_model','articule',
                        'price_RUB','price_USD','active','created_at','updated_at'];
}
