<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class BrandPicture extends SmartModel
{
    public static $files = array(
        'image' => array(
            'sizes' => array(
                'list' => array(
                    'size' => '90x70',
                    'process' => 'fitOut',
                ),
            ),
        )
    );

    public function getImageUrlAttribute()
    {
        $this->loadFiles();
        if (count($this->image['sizes']['list']['link'])){
            return asset($this->image['sizes']['list']['link']);
        }
        return asset('img/brands/01.png');
    }

    public function getImageLinkAttribute()
    {
        $this->loadFiles();
        if (count($this->image['sizes']['list']['link'])){
            return asset($this->image['link']);
        }
        return asset('img/brands/01.png');
    }
}
