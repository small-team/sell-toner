<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class Brand extends SmartModel
{
    protected $fillable = ['id','title','_slug','_position','meta_title','meta_description',
        'meta_keywords','og_title','og_description','created_at','updated_at'];
}
