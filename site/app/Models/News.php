<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use SmartModel\SmartModel;

class News extends SmartModel

{
    protected $news_tag_ids = [];
    protected $appends = ['tags_as_string',];

    public $rules = array(
        'title' => 'required',
//        '_slug' => 'required',
    );

    public static $files = array(
        'image' => array(
            'sizes' => array(
                'list' => array(
                    'size' => '161x108',
                    'process' => 'fitOut',
                ),
            ),
        ),
        'image_poster' => array(
            'sizes' => array(
                'poster'=>array(
                    'size' => '408x330',
                    'process' => 'fitOut',
                ),
            ),
        ),
        'og_image' => array(
            'name' => 'native',
        ),
    );

    public  function getTagsAsStringAttribute(){
        $id_news    =   $this->id;
        return (implode(",",\DB::table('tags')
                            ->select('title')
                            ->whereIn('tags.id', \DB::table('tags_to_news')
                                                ->select('id_tag')
                                                ->where('id_news',$id_news)->lists('id_tag'))
                            ->lists('title') ));
    }

    public  function getDateAttribute(){
        return Carbon::parse($this->created_at)->format('d.m.Y');
    }
    
    public  function getImageUrlAttribute(){
        $this->loadFiles();
        if (count($this->image['sizes']['list']['link'])){
            return asset($this->image['sizes']['list']['link']);
        }
        return asset('img/products/01.png');
    }

    public  function getBigImgAttribute(){
        $this->loadFiles();
        if (count($this->image_poster['sizes']['poster']['link'])){
            return asset($this->image_poster['sizes']['poster']['link']);
        }
        return asset('img/image.png');
    }



    public function afterSave() {
        parent::afterSave();
        $id_news = $this->id;
        $id_tags_for_news =  $this->news_tag_ids;
        \DB::table('tags_to_news')->where('id_news',$id_news)->delete();
        $tags_this_news=[];
        foreach($id_tags_for_news as $val){
            $tags_this_news[]=['id_news' =>  $id_news,
                                'id_tag'  =>  $val];
        }
//        dd($tags_this_news);
        \DB::table('tags_to_news')->insert($tags_this_news);

    }

    public  function setTagsAsStringAttribute($val){
        $tags_this_news =  explode(',', $val);
        foreach ($tags_this_news as &$_tag) {
            $_tag = trim($_tag);
        }

        $existing_tags   =    \DB::table('tags')->whereIn('title',$tags_this_news)->lists('title','id');
        $id_existing_tags   =   array_keys($existing_tags);
        $tile_existing_tags =   array_values($existing_tags);
//get tags that still do not have
        $non_existent_tags  =   array_diff( $tags_this_news, $tile_existing_tags);
//form a list of the id record
        $id_tags_for_news  = $id_existing_tags;
//add new tag
        foreach($non_existent_tags as $tag){
            $id_tags_for_news[] = \DB::table('tags')->insertGetId(['title'=>$tag]);
        }
//remove all tags for this news
//        \DB::table('tags_to_news')->where('id_news',$id_news)->delete();

////        $tags_this_news=[];
//        foreach($id_tags_for_news as $val){
//            $tags_this_news[]=['id_news' =>  $id_news,
//                              'id_tag'  =>  $val];
//        }
        $this->news_tag_ids = $id_tags_for_news;
//        write new tags for news

//        \DB::table('tags_to_news')->insert($tags_this_news);
    }

    public static function getNewNews() {
        return \DB::table('news')
            ->where('is_active','=',1)->whereNotNull('_slug')->where('_slug','<>','')
            ->limit(3)->orderBy('created_at','desc')->get();
    }

    public static function getNewsPaginationList($on_page = 10)
    {
        $news = News::where('is_active','=',1)->whereNotNull('_slug')->where('_slug','<>','')
                ->orderBy('created_at','desc')
                ->paginate($on_page);

        return $news;
    }

    public static function getNewsByIds($ids)
    {
        $news = News::where('is_active','=',1)->whereNotNull('_slug')->where('_slug','<>','')
                ->whereIn('id',$ids)
                ->orderBy('created_at','desc')
                ->get();

        return $news;
    }

    public static function getLikeNews($id_news)
    {
        $news_top = \DB::table('tags_to_news')
            ->select(\DB::raw('count(*) as count, id_news'))
            ->join('news', 'id_news', '=', 'news.id')
            ->where('is_active','=',1)->whereNotNull('_slug')->where('_slug','<>','')
            ->whereIn('id_tag',\DB::table('tags_to_news')
                ->where('id_news',$id_news)->lists('id_tag'))
            ->groupBy('id_news')
            ->orderBy('count','desc')
            ->limit(5)->get();

        $news_ids = [];
        foreach ($news_top as $nt) {
            if ($nt->id_news != $id_news) {
                $news_ids[] = $nt->id_news;
            }
        }

        return self::getNewsByIds($news_ids);
//
//        return \DB::table('tags_to_news')
//            ->select(\DB::raw('count(*) as count, id_news, news.title, news.content,news.preview, news.created_at,news.id'))
//            ->join('news', 'id_news', '=', 'news.id')
//            ->where('is_active','=',1)
//            ->whereIn('id_tag',\DB::table('tags_to_news')
//                ->where('id_news',$id_news)->lists('id_tag'))
//            ->groupBy('id_news')
//            ->orderBy('count','desc')
//            ->limit(5)->get();
    }
}
