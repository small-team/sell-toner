<?php
namespace Admin\Controllers;

use App\Models\Admin;
use Illuminate\Support\Facades\Redirect;
use MaxMind\WebService\Http\Request;

/**
 * @author Max Kovpak <max_kovpak@hotmail.com>
 * @copyright SmallTeam (c) 2014
 */

class AdminIndexController extends AdminBaseController {
    public function getIndex() {
        $this->view->setTemplate('admin::index');
        return $this->view->make($this);
    }

    public function anyLogin() {

        if(Admin::isLoggedIn()) {
            return \Redirect::to('admin/');
        }
//        $this->view->setLayoutTemplate('admin::default');
        $this->view->setTemplate('admin::login');

        $login = isset($_POST['login']) ? trim(strip_tags($_POST['login'])) : false;
        $pass = isset($_POST['password']) ? $_POST['password'] : false;

        \Log::info($login.$pass);

//
        if($login && $pass) {
            if(Admin::login($login, $pass)) {
                return \Redirect::to('admin/');
            } else {
                $this->view->error = 'Логин или пароль введены не верно';
            }
        }

        return $this->view->make($this);
    }

    public function anyLogout() {
        Admin::logout();
        return Redirect::to('admin/login');
    }

    public function to_login(){
        dd(121312312);
    }

}