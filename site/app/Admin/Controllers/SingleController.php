<?php
namespace Admin\Controllers;

use Illuminate\Database\Eloquent\Model as Eloquent,
    Illuminate\Support\Facades\DB,
    Illuminate\Support\Facades\Input,
    Illuminate\Support\Facades\Redirect;

use App\Model;
use Admin\Tools\ViewHelper;
use SmartModel\SmartModel as SmartModel;

/**
 * @author Max Kovpak <max_kovpak@hotmail.com>
 * @copyright SmallTeam (c) 2014
 */
 
class SingleController extends AdminBaseController {

    public function anyIndex() {
        if($update_field = Input::get('update_field')){
            $this->view->setTemplate('admin::list._info_update_field');
            $this->view->update_field = $update_field;
            $this->view->setRenderType(ViewHelper::RENDER_STANDALONE);
        }else{
            $this->view->setTemplate('admin::list._info');
        }

        /* @var SmartModel $object */
        $model = $this->_module['model'];

        $class_name = 'App\\Models\\'.$model;

        $object = new $class_name();
        $object = $object->findOrNew((isset($this->_module['key']) ? $this->_module['key'] : 1));

        if ($data = Input::get('data')) {
            $this->beforeSaveProcessData($data);
            $this->requireAction('edit');
            $this->mergeData($object, $data);

            if(!$object->save()) {
                $this->view->_errors = $object->errors();
                $this->view->setMessage(array('type'=>'error','text'=>'Во время сохранения произошли ошибки'));
            } else {
                $this->view->setMessage(array('type'=>'success','text'=>'Изменения сохранены!'));
                $this->afterSuccessfulSave($data, $object, false);
                return Redirect::to('admin/' . $this->_module_name . '/');
            }
        }

        if($object || is_object($object)) {
            $object->loadFiles();
        }

        $object = $object->toArray();
        $this->view->object = $object;
        return $this->view->make($this);
    }

}
