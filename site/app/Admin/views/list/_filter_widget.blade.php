<div class="jarviswidget jarviswidget-color-blueDark" id="widget-filter" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false" role="widget" style="">
    <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="#" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> </div><div class="widget-toolbar" role="menu"><a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul></div>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2>
            @if(isset($module['filter_title']) && !empty($module['filter_title']))
                {{$module['filter_title']}}
            @else
                {{'Фильтр'}}
            @endif
        </h2>
        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
    <div role="content">
        <form id="filter-data-form" method="post" @if (isset($_toggle_admin_elements['filter']) && $_toggle_admin_elements['filter']===false ){{'style="display: none;"'}}@endif>
            <div id="filter-to-hide" >
                <table class="table">
                    @foreach ($module['filters'] as $field_name => $field_info)
                        <tr>
                            <td class="tar vam filter-item-title">{{ $field_info['title'] }}</td>
                            @if($field_info['title'] = 'Телефон')
                                <script>
                                    $("[name='filters[phone]']").mask("+7 (999) 999-9999");
                                </script>
                            @endif
                            <td>
                                <?php $field_input_name = "filters[".($field_name)."]"; ?>
                                <?php $field_value = (isset($filter_values[$field_name]) ? $filter_values[$field_name] : null); ?>
                                @include ("admin::list._elements.filters.".$field_info['type'])
                            </td>
                        </tr>
                    @endforeach
                </table>


            </div>
            <div class="widget-footer ">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="btn-group  pull-left">
                            <input class="btn btn-info" type="submit" value="Фильтр"/>
                            <a style="" class="btn btn-danger" href="/{{$app_name}}/{{$module['name']}}/?filter_reset=true">Сброс</a>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        </form>
    </div>
</div>
