<div class="editable_field_block">
    <select class="form-control editable_field input_text" name="{{ $field_input_name }}">
    <option value="">все значения</option>
    @foreach ($field_info['items'] as $select_key => $select_title)
        <option value="{{ $select_key }}" @if ($select_key == $field_value && $field_value != null) selected="selected" @endif>{{ $select_title }}</option>
    @endforeach
    </select>
</div>