@if (isset($field_info['timepicker']))
    <script>
        var timePicker = true;
    </script>
    <?php $format = "d-m-Y H:i"; ?>
@else
    <script>
        var timePicker = false;
    </script>
    <?php $format = "d-m-Y"; ?>
@endif

<script type="text/javascript" src="/assets/admin/datepicker/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="/assets/admin/datepicker/css/bootstrap-datetimepicker.min.css">
<script>
    $(document).ready(function(){
        $('.datetimepicker-range-filter').datetimepicker({
            language: 'ru',
            pickTime: timePicker,
            pick12HourFormat: false,
            format: timePicker ? 'yyyy-MM-dd hh:mm' : 'yyyy-MM-dd',
            weekStart: 1
        });
    });
</script>

<div class="editable_field_block datepicker_holder fleft mr10 input-group" style="display: flex;">
    <div class="datetimepicker-range-filter  input-group" style="width: 220px; margin-right: 10px;">
        <div class="input_disable_container">
            <input style="width: 200px" data-format="@if (isset($field_info['timepicker']))dd.MM.yyyy hh:mm:ss @else dd.MM.yyyy @endif"  class="form-control @if (isset($field_info['timepicker'])) timepicker_input @endif datepicker_input" name="{{$field_input_name}}[from]" type="text" value="@if(!empty($field_value['from'])){{$field_value['from']}}@endif"/>
            <div class=""></div>
        </div>
        <span class="btn btn-default add-on input-group-addon"><i class="fa-calendar fa"></i></span>
    </div>

    <div class="datetimepicker-range-filter  input-group" style="width: 220px;">
        <div class="input_disable_container">
            <input style="width: 200px" data-format="@if (isset($field_info['timepicker']))dd.MM.yyyy hh:mm:ss @else dd.MM.yyyy @endif"  class="form-control @if (isset($field_info['timepicker'])) timepicker_input @endif datepicker_input" name="{{$field_input_name}}[to]" type="text" value="@if(!empty($field_value['to'])){{$field_value['to']}}@endif"/>
            <div class=""></div>
        </div>
        <span class="btn btn-default add-on input-group-addon"><i class="fa-calendar fa"></i></span>
    </div>
</div>