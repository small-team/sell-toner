<a class="readonly_link" target="_blank" href="{{$field_value}}">{{{isset($field_info['label']) ? $field_info['label'] : $field_value}}}</a>
@if (isset($field_info['description']))
    <div class="input_description"><small>@if (isset($field_info['description']))<i class="fa fa-info">&nbsp;&nbsp;</i>{{$field_info['description']}}@endif</small></div>
@endif