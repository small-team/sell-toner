<?php $select_value=(isset($field_info['local_field']) && isset($object[$field_info['local_field']]) ? $object[$field_info['local_field']] : $field_value); ?>
@if (isset($field_info['items'][$select_value]))
    @if (isset($field_info['show_icon_value']) && $field_info['show_icon_value'] &&
         isset($field_info['icons']) && count($field_info['icons']) &&
         isset($field_info['icons'][$select_value])
     )
        <img src="{{$field_info['icons'][$select_value]}}" alt="{{ $field_info['items'][$select_value] }}"/>
    @else
        <span class="readonly_text_field">{{ $field_info['items'][$select_value] }}</span>
    @endif
@else
    @if (isset($field_info['undefined_text']))
    <span class="readonly_text_field">{{ $field_info['undefined_text'] }}</span>
    @else
    <span class="readonly_text_field">неопределено</span>
    @endif
@endif