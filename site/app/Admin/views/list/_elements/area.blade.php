<div class="editable_field_block @if($field_error) has-error @endif">
    <textarea class="editable_field input_text form-control" style="height:300px;" name="{{$field_input_name}}">{{$field_value}}</textarea>
    <div class="errors_block">
    @if (isset($field_error) && is_array($field_error))
        @foreach($field_error as $item_error)
        <div class="text-danger note note-error">{{$item_error}}</div>
        @endforeach
    @endif
    </div>
    <div class="input_description"><small>@if (isset($field_info['description']))<i class="fa fa-info">&nbsp;&nbsp;</i>{{$field_info['description']}}@endif</small></div>
</div>