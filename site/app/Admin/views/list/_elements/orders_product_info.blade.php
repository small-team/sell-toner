@if(!empty($field_value))
<table class="table" border="1px">
    <thead>
    <tr>
            <td>Артикул</td>
            <td>Модель</td>
            <td>Количество</td>
            <td>Цена</td>
            <td>Сумма</td>
    </tr>
    </thead>
    @if(isset($field_value) && $field_value)
        @foreach($field_value as $product)
                    <tr>
                        <td>{{$product->articule}}</td>
                        <td>{{$product->model}}</td>
                        <td>{{$product->quantity}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->sum}}</td>
                    </tr>
        @endforeach
    @endif
</table>
@else
<table border="1px">
    <thead>
        <tr>Товаров нет</tr>
    </thead>
</table>
@endif