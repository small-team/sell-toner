@if ($module['action_name'] == 'add' && isset($field_info['checked']) && $field_info['checked'])
    <?php $field_value = true ?>
@endif
<div class="editable_field_block {{{ $field_error ? 'has-error' : '' }}} checkbox_container">
    <input id="checkbox-handler-{{$field_name}}" class="checkbox_handler" type="checkbox" {{{ $field_value ? 'checked="checked"' : '' }}}/>

    <input id="checkbox-input-{{$field_name}}" class="editable_field" type="hidden" name="{{$field_input_name}}" value="{{$field_value}} " />
    <div class="errors_block">
    @if ($field_error)
        @foreach ($field_error as $item_error)
        <div class="input_error">{{$item_error}}</div>
        @endforeach
    @endif
    </div>
</div>