<?php
return [
    'title' => 'На модерации',
    'model' => 'Questions',
    'per_page' => 50,
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'name' => [
            'title' => 'Имя',
            'type' => 'text',
        ],
        'city' => [
            'title' => 'Город',
            'type' => 'text',
        ],
        'IP_user' => [
            'title' => 'IP_user',
            'type' => 'text',
        ],
        'question' => [
            'title' => 'Вопрос',
            'type' => 'area',
        ],
        'is_moderate' => [
            'title' => 'Проверено',
            'type' => 'checkbox',
        ],
        'title' => [
            'title' => 'Название вопроса',
            'type' => 'text',
        ],
        'position' => [
            'title' => 'Позиция',
            'type' => 'text',
        ],
        'answer_author' => [
            'title' => 'Имя модератора',
            'type' => 'text',
        ],
        'answer_position' => [
            'title' => 'Должность модератора',
            'type' => 'text',
        ],
        'answer' => [
            'title' => 'Ответ',
            'type' => 'area',
        ],
        'created_at' => [
            'title' => 'Время создания',
            'type' => 'text',
        ],
    ],
    'sort' => 'id DESC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','name','city','question','IP_user','created_at']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],

        'delete' => true
    ],
    'filters' => [
        'created_at' => [
            'title' => 'Дата/Время заявки',
            'type' => 'datepicker_range',
        ],
        'IP_user' => [
            'title' => 'IP_user',
            'type' => 'text',
        ],
    ],
    'group_actions' => [
        'ca_delete_all' => [
            'confirm' => true,
            'title' => 'Удалить все',
        ],
        'ca_approve_all' => [
            'confirm' => true,
            'title' => 'Одобрить все',
        ],
    ],
]
    ;