<?php

use Admin\Tools\StringTools;
use Illuminate\Database\Eloquent\Model as Eloquent,
    Illuminate\Support\Facades\DB,
    Illuminate\Support\Facades\Input,
    Illuminate\Support\Facades\Redirect,
    Illuminate\Support\Facades\Session,
    Illuminate\Support\Facades\File,
    SmartModel\SmartModel as SmartModel,
    Admin\Tools\ArrayTools,
    Admin\Tools\ViewHelper;
use Illuminate\Pagination\Paginator;
/**
 * @date: 09.08.2014 15:15:13
 */
class NewsController extends Admin\Controllers\ListController{

    public function anyEdit($id) {
        $this->requireAction('edit');
        if($update_field = Input::get('update_field')){
            $this->view->setTemplate('admin::list._info_update_field');
            $this->view->update_field = $update_field;
            $this->view->setRenderType(ViewHelper::RENDER_STANDALONE);
        }else{
            $this->view->setTemplate('admin::list._info');
        }
//        $model = $this->_module['model'];
        $class_name = 'App\\Models\\'.$this->_module['model'];

        /* @var SmartModel $object */
        $object = $class_name::find($id);
        if (!$object) return Redirect::to('admin/' . $this->_module_name . '/');

        if ($data = Input::get('data')) {
            if(empty($data['_slug'])){
                $data['_slug'] = Slug::make(mb_strtolower($data['title']));
                $data['_slug'] = preg_replace('#[^A-Za-z0-9 \-]#u', '', $data['_slug']);
            }
            $this->beforeSaveProcessData($data);
            $this->mergeData($object, $data);
            $redirect_url = 'admin/' . $this->_module_name . (Input::get('back_to_list') ? ' ' : ('/edit/'.$object->{$this->_key}.'/'));

            if(!$object->save()) {
                $this->view->_errors = $object->errors();
                $this->view->setMessage(array('type'=>'error','text'=>'Во время сохранения произошли ошибки'));
            } else {
                $this->view->setMessage(array('type'=>'success','text'=>'Изменения сохранены!'));

                $this->afterSuccessfulSave($data, $object);
                return Redirect::to($redirect_url);
            }
        }
//dd($object->loadFiles()->toArray());
        $this->view->object = $object->loadFiles()->toArray();
        return $this->view->make($this);
    }

    /**
     * @description Add item action
     * */
    public function anyAdd() {
        $this->requireAction('add');
        $this->view->setTemplate('admin::list._info');
        $model = $this->_module['model'];
        if ($data = Input::get('data')) {
            if(empty($data['_slug'])){
                $data['_slug'] = Slug::make(mb_strtolower($data['title']));
                $data['_slug'] = preg_replace('#[^A-Za-z0-9 \-]#u', '', $data['_slug']);
            }
            $this->beforeSaveProcessData($data);
            /* @var SmartModel $object */
            $class_name = 'App\\Models\\'.$model;
            $object = new $class_name();

            $this->mergeData($object, $data);

            if(!$object->save() || !intval($object->{$this->_key})) {
                $this->view->object = $object;
                $this->view->_errors = $object->errors();
                $this->view->setMessage(array('type'=>'error','text'=>'Во время сохранения произошли ошибки'));
            } else {
                $redirect_url = 'admin/' . $this->_module_name . (Input::get('back_to_list') ? ' ' : ('/edit/'.$object->{$this->_key}.'/'));
                $this->view->setMessage(array('type'=>'success','text'=>'Обьект успешно добавлен!'));
                $object->loadFiles();
                $this->afterSuccessfulSave($data, $object);
                return Redirect::to($redirect_url);
            }
        }
        return $this->view->make($this);
    }
}
