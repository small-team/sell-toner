<?php
return [
    'title' => 'Новости и статьи',
    'model' => 'News',
    'fields' => [
        'id' => [
            'title' => 'ID',
            'type' => 'text',
        ],
        'is_active' => [
            'title' => 'Активна',
            'type' => 'checkbox',
            'checked'=>true
        ],
        '_slug' => [
            'title' => 'Ссылка',
            'type' => 'text',
        ],
        'created_at' => [
            'title' => 'Дата',
            'type' => 'datepicker',
            'timepicker' => true
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
        ],
        'preview' => [
            'title' => 'Анонс',
            'type' => 'area',
        ],
        'content' => [
            'title' => 'Контент',
            'type' => 'rich',
        ],
        'image' => [
            'title' => 'Изображение в списке',
            'type' => 'image',
            'description'=>'Выберите изображение размером 161x108'
        ],
        'image_poster' => [
            'title' => 'Основное изображение для новости',
            'type' => 'image',
            'description'=>'Выберите изображение размером 408x330'
        ],
        'meta_title' => [
            'title' => 'Meta_title',
            'type' => 'text',
        ],
        'meta_description' => [
            'title' => 'Meta_description',
            'type' => 'text',
        ],
        'meta_keywords' => [
            'title' => 'Meta_keywords',
            'type' => 'text',
        ],
        'og_title' => [
            'title' => 'Social title',
            'type' => 'text',
        ],
        'og_description' => [
            'title' => 'Social description',
            'type' => 'text',
        ],
        'og_image' => [
            'title' => 'SocialImage',
            'type' => 'image',
            'description' => 'Изображение для шаринга в соц. сетях',
        ],
        'tags_as_string' => [
            'title' => 'Теги',
            'type' => 'area',
            'virtual' => true,
            'description'=>'Введите теги для новости через запятую'
        ],


    ],
    'sort' => 'id DESC',
    'tabs' => [
        'metas'=>['title'=> 'Метаданные','fields'=> ['meta_title', 'meta_description', 'meta_keywords', 'og_title', 'og_description', 'og_image']],
    ],
    'actions' => [
        'list' => [
            'show' => ['id', 'title', 'is_active', 'created_at']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
];