<?php
/**
 * @date: 09.08.2014 15:15:13
 */
class AclController extends Admin\Controllers\SingleController{

    public function anyIndex() {

        if($update_field = Input::get('update_field')){
            $this->view->setTemplate('admin::list._info_update_field');
            $this->view->update_field = $update_field;
            $this->view->setRenderType(ViewHelper::RENDER_STANDALONE);
        }else{
            $this->view->setTemplate('admin::list._info');
        }

        /* @var SmartModel $object */
        $model = $this->_module['model'];

        $class_name = 'App\\Models\\'.$model;

        $object = new $class_name();
        $object = $object->findOrNew((isset($this->_module['key']) ? $this->_module['key'] : 1));

        if ($data = Input::get('data')) {
            $login  =   $data['login'];
            $password  =   $data['password'];
            $re_password  =   $data['re_password'];

            if(isset($password) && isset($re_password) && !empty($password) && !empty($re_password)) {
                if(sha1($password) != sha1($re_password)) {
                    $this->view->setMessage(array('type' => 'error', 'text' => 'Пароли не совпадают'));

                } else {
                    $object->password = sha1($data['password']);
                    $object->save();
                    $this->view->setMessage(array('type'=>'success','text'=>'Изменения сохранены!'));
                }
            }

        }

        $object = $object->toArray();
        $this->view->object = $object;
        return $this->view->make($this);
    }


}
