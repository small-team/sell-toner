<?php
return [
    'title' => 'Популярные предложения',
    'model' => 'Popular',
    'fields' => [
        'id' => [
            'title' => 'ID',
            'type' => 'text',
        ],
        'title' => [
            'title' => 'Название',
            'type' => 'text',
        ],
        'price' => [
            'title' => 'Стоимость',
            'type' => 'text',
        ],
        
        'slug' => [
            'title' => 'Ссылка',
            'type' => 'text',
        ],
        
        
        'image' => [
            'title' => 'Изображение',
            'type' => 'image',
            'description'=>'Выберите изображение размером 110x106'
        ],


    ],
    'sort' => 'id DESC',
    
    'actions' => [
        'list' => [
            'show' => ['id', 'title', 'price']
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
];