<?php

use Admin\Tools\StringTools;
use Illuminate\Database\Eloquent\Model as Eloquent,
    Illuminate\Support\Facades\DB,
    Illuminate\Support\Facades\Input,
    Illuminate\Support\Facades\Redirect,
    Illuminate\Support\Facades\Session,
    Illuminate\Support\Facades\File,
    SmartModel\SmartModel as SmartModel,
    Admin\Tools\ArrayTools,
    Admin\Tools\ViewHelper;
use Illuminate\Pagination\Paginator;
/**
 * @date: 09.08.2014 15:15:13
 */
class PopularController extends Admin\Controllers\ListController{

    public function loadSlugItems() {
        $items = \App\Models\Brand::all(array('id','_slug'))->keyBy('id')->toArray();
        $list = array();
        foreach ($items as $id=>$items) {
            $list[$items['_slug']] = $items['_slug'];
        }
        return $list;
    }
}
