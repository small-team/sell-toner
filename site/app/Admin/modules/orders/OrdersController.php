<?php
/**
 * @date: 09.08.2014 15:15:13
 */

use Illuminate\Pagination\Paginator;

class OrdersController extends Admin\Controllers\ListController{

    public function anyIndex($page_number = 1) {
        /* @var SmartModel $obj */
        $class_name = 'App\\Models\\'.$this->_module['model'];

        $obj = new $class_name;
        $table = $obj->getTable();
        $query = DB::table($table);

        $this->view->setTemplate('admin::list.orders');

        $sort = $this->getSortCriteria();
        if(is_object($sort)) {
            return Redirect::to('admin/' . $this->_module_name . '/');
        }
        $this->getFilterCriteria($query);

        if(isset($sort['field']) && !empty($sort['field'])) {
            if ($sort['dir'] !== false) {
                $sort['dir'] = $sort['dir'] && in_array($sort['dir'], array('asc', 'desc')) ? $sort['dir'] : 'asc';
                $query->orderByRaw($sort['field'].' '.$sort['dir']);
            } else {
                $query->orderByRaw($sort['field']);
            }
        }

        if ($per_page = $this->getModuleParam('per_page')) {
            $currentPage = $page_number;
            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $this->view->count_orders = count($query->get());

            $table_data = $query->paginate($per_page);

            $this->view->pager = array(
                'current_page' => $table_data->currentPage(),
                'on_page' => $table_data->perPage(),
                'pages_count' => $table_data->lastPage(),
                'results_count' => $table_data->total(),
            );
            $table_data = $table_data->items();

            if(is_array($table_data)) {
//                $model = $this->_module['model'];
                foreach ($table_data as &$item) {
                    $item = $class_name::find($item->{$this->_key});
                    $item = is_object($item) ? $item->loadFiles()->toArray() : array();
                }
            }
            $this->view->link = 'admin/'.$this->_module_name.'/';
            $this->view->pre_page_link = 'page/';
            $this->setModuleSessionParam('page',$this->view->pager['current_page']);
        } else {
            $this->view->count_orders = count($query->get());
            $table_data = $query->get();
            $table_data = \Admin\Tools\ArrayTools::useValue($table_data, $this->_key);
            if(is_array($table_data)) {
                $data = array();
//                $model = $this->_module['model'];
                foreach ($table_data as $k => $id) {
                    /* @var SmartModel $object */
                    $object = $class_name::find($id);
                    $data[$k] = $object->loadFiles()->toArray();
                }
                $table_data = $data;
            }
        }

        if (!$table_data) $this->view->table_data = array();
        $this->view->table_data = $table_data;
        return $this->view->make($this);
    }

    public function anyEdit($id) {
        $this->requireAction('edit');
        if($update_field = Input::get('update_field')){
            $this->view->setTemplate('admin::list._info_update_field');
            $this->view->update_field = $update_field;
            $this->view->setRenderType(\Admin\Tools\ViewHelper::RENDER_STANDALONE);
        }else{
            $this->view->setTemplate('admin::list._info');
        }

        $class_name = 'App\\Models\\'.$this->_module['model'];

        /* @var SmartModel $object */
        $object = $class_name::find($id);

        if (!$object) return Redirect::to('admin/' . $this->_module_name . '/');

        if ($data = Input::get('data')) {
            $this->beforeSaveProcessData($data);
            $this->mergeData($object, $data);
            $redirect_url = 'admin/' . $this->_module_name . (Input::get('back_to_list') ? ' ' : ('/edit/'.$object->{$this->_key}.'/'));

            if(!$object->save()) {
                $this->view->_errors = $object->errors();
                $this->view->setMessage(array('type'=>'error','text'=>'Во время сохранения произошли ошибки'));
            } else {
                $this->view->setMessage(array('type'=>'success','text'=>'Изменения сохранены!'));

                $this->afterSuccessfulSave($data, $object);
                return Redirect::to($redirect_url);
            }
        }

        $this->view->object = $object->loadFiles()->toArray();
        return $this->view->make($this);
    }

}
