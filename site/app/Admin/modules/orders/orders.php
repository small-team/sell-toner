<?php
return [
    'title' => 'Журнал заявок',
    'model' => 'CartridgeOrder',
    'per_page' => 50,
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
        ],
        'created_at' => [
            'title' => 'Дата/Время заявки',
            'type' => 'datepicker',
            'timepicker' => true,
            'readonly'=> true,
            'sortable'=> true
        ],
        'typecode' => [
            'title' => 'Тип заявки / № Заявки',
            'delimiter' => '<br/><br/>',
            'type' => 'fieldsgroup',
            'fields' => [
                'type',
                'code',
            ]
        ],
        'type' => [
            'title' => 'Тип заявки',
            'type' => 'select',
            'readonly' => true,
            '_in_group' => true,
            'items'=>[
                        'quick_request'=>'Быстрая заявка с главной',
                        'retain_request'=>'Заявка с меню Оставить заявку',
                        'call_back'=>'Заявка на обратный звонок',
                        'cart_request'=>'Заявка с корзины'
                     ],
            'show_icon_value' => true,
            'icons' => [
                'quick_request'  => '/img/quick_request.png',
                'retain_request' => '/img/retain_request.png',
                'call_back'      => '/img/call_back.png',
                'cart_request'   => '/img/cart_request.png'
            ]
        ],
        'code' => [
            'title' => '№ Заявки',
            'type' => 'text',
            'readonly' => true,
            '_in_group' => true,
        ],
        'fio' => [
            'title' => 'ФИО',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'city' => [
            'title' => 'Город',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'phoneemail' => [
            'title' => 'Телефон / Email',
            'type' => 'fieldsgroup',
            'delimiter' => '<br/><br/>',
            'fields' => [
                'phone',
                'email',
            ]
        ],
        'phone' => [
            'title' => 'Телефон',
            'type' => 'text',
            'readonly'=>'true',
            '_in_group' => true,
        ],
        'email' => [
            'title' => 'Email',
            'type' => 'text',
            'readonly'=>'true',
            '_in_group' => true,
        ],
        'comment' => [
            'title' => 'Что вы хотите нам предложить',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'orders_product_info' => [
            'title' => 'Товары в заказе',
            'type' => 'orders_product_info',
        ],
        'sum' => [
            'title' => 'Стоимость заказа, руб.',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'quantity' => [
            'title' => 'Кол.во в заказе, шт',
            'type' => 'text',
            'readonly'=>'true'
        ],
        'order_photos_links' => [
            'title' => 'Фотографии',
            'type' => 'orders_photos',
        ],
    ],
    'sort' => 'created_at DESC',
    'tabs' => ['order_info'=>['title'=> 'Информация о товарах в заказе','fields'=> ['sum', 'quantity', 'orders_product_info']],
    ],
    'actions' => [
      'list' => [
          'show' => ['id','typecode','phoneemail','type','code','fio','city','phone','email','orders_product_info','created_at','sum','quantity','order_photos_links',]
      ],
      'edit' => [
        'hide' => ['id','typecode','phoneemail'],
      ],
      'delete' => true
    ],
    'filters' => [
        'created_at' => [
            'title' => 'Дата/Время заявки',
            'type' => 'datepicker_range',
        ],
        'phone' => [
            'title' => 'Телефон',
            'type' => 'text',
        ],
        'type' => [
            'title' => 'Тип заявки',
            'type' => 'select',
        ],
    ]
]
;