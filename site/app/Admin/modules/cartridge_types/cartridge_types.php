<?php
return [
    'title' => 'Типы картриджей',
    'model' => 'CartridgeType',
    'fields' => [
        'id' => [
            'title' => 'ID',
            'type' => 'text',
            'readonly'=> 'true'
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
            'readonly'=> 'true'
        ],

    ],
    'sort' => 'id ASC',
    'tabs' => [],
    'actions' => [
        'list' => [
            'show' => ['id','title']
        ],
        'edit' => true
    ]
]
    ;