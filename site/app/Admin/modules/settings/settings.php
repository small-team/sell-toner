<?php
return [
    'title' => 'Настройки сайта',
    'model' => 'Settings',
    'fields' => [
        'edb_uri' => [
            'title' => 'Ссылка на ЕДБ',
            'type' => 'text',
        ],
        'time_display_phones' => [
            'title' => 'Отображение телефонов',
            'type' => 'select',
            'items'=>[
                'on'=>'Телефон отображается постоянно',
                'off'=>'Отключить отображение',
                'period'=>'Отображать телефон в период',
            ],
        ],
        'from_time' => [
            'title' => 'Отображать телефон с',
            'type' => 'timepicker',
        ],
        'before_time' => [
            'title' => 'по',
            'type' => 'timepicker',
        ],
        'satellite_key' => [
            'title' => 'Ключ доступа',
            'type' => 'text',
        ],
        'data_exchange' =>[
            'title'=>'Обмен данными',
            'type'=>'btn',
            'btn_type'=>'success',
            'btn_title'=>'Запустить',
            'link'=>'/data_exchange'
        ],
        'main_page_content' => [
            'title' => 'Текст для главной страницы',
            'type' => 'rich',
        ],
//        'timeblade' => [
//            'title' => 'График работы компании в Head',
//            'type' => 'rich',
//        ],
        'company_phone' => [
            'title' => 'Телефон компании в Head',
            'type' => 'text',
        ],
        'company_email' => [
            'title' => 'Email компании в Head',
            'type' => 'text',
        ],
//        'company_address' => [
//            'title' => 'Адрес компании в Head',
//            'type' => 'rich',
//        ],
//        'timeblade_footer' => [
//            'title' => 'График работы компании в Footer',
//            'type' => 'text',
//        ],
        'company_phone_footer' => [
            'title' => 'Телефон компании в Footer',
            'type' => 'text',
        ],
        'footer_email' => [
            'title' => 'Email компании в Footer',
            'type' => 'text',
        ],
        'footer_year' => [
            'title' => 'Год в Footer',
            'type' => 'text',
        ],
//        'footer_address' => [
//            'title' => 'Адрес компании в Footer',
//            'type' => 'text',
//        ],


        'copyright' => [
            'title' => 'Копирайт',
            'type' => 'rich',
        ],
        'vk' => [
            'title' => 'Код для ВКонтакте',
            'type' => 'text',
        ],
        'fb' => [
            'title' => 'Код для Facebook',
            'type' => 'text',
        ],
        'google' => [
            'title' => 'Код для Google+',
            'type' => 'text',
        ],
        'twitter' => [
            'title' => 'Код для Twitter',
            'type' => 'text',
        ],
        'content_error' => [
            'title' => '404',
            'type' => 'rich',
        ],
        'metrika' => [
            'title' => 'Код для Яндекс метрики',
            'type' => 'area',
        ],
//        'best_deals' => [
//            'title' => 'Заголовок',
//            'type' => 'rich',
//        ],
        'about_address' => [
            'title' => 'Адресс',
            'type' => 'text',
        ],
        'about_contact' => [
            'title' => 'Телефоны',
            'type' => 'text',
        ],
        'about_timeblade' => [
            'title' => 'Время работы',
            'type' => 'text',
        ],
        'about_email' => [
            'title' => 'Email',
            'type' => 'text',
        ],
        'about_coordinates_l' => [
            'title' => 'Координаты - широта',
            'type' => 'text',
        ],
        'about_coordinates_d' => [
            'title' => 'Координаты - долгота',
            'type' => 'text',
        ],
//        'content_car_price' => [
//            'title' => 'Контент прайсов',
//            'type' => 'rich',
//        ],
        'robots' => [
            'title' => 'robots.txt',
            'virtual' => true,
            'type' => 'area',
        ],
        'sitemap' => [
            'title' => 'sitemap.xml',
            'virtual' => true,
            'type' => 'area',
        ],

        'm_viber' => [
            'title' => 'Viber',
            'type' => 'text',
        ],
        'm_whatsapp' => [
            'title' => 'Whatsapp',
            'type' => 'text',
        ],
        'm_telegram' => [
            'title' => 'Telegram',
            'type' => 'text',
        ],
        'm_skype' => [
            'title' => 'Skype',
            'type' => 'text',
        ],

    ],
    'sort' => 'id ASC',
    'tabs' => [
                'main_page'=>['title'=> 'Текст на главной','fields'=> ['main_page_content']],
                'content_error'=>['title'=> '404','fields'=> ['content_error']],
                'footer'=>['title'=> 'Футер','fields'=> ['copyright','timeblade_footer',
                                                        'company_phone_footer','footer_email','footer_address','footer_year',]],
                'head'=>['title'=> 'Head (Шапка)','fields'=> ['timeblade','company_phone','company_email','company_address','']],
                'contacts'=>['title'=> 'Контакты','fields'=> ['m_viber','m_whatsapp','m_telegram','m_skype',]],
                'robots'=>['title'=> 'Robots.txt','fields'=> ['robots']],
                'sitemap'=>['title'=> 'Sitemap.xml','fields'=> ['sitemap']],


    ],
    'actions' => [
        'list' => [],
        'add' => [
            'hide' => ['id'],
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;