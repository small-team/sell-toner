<?php
/**
 * @date: 09.08.2014 15:15:13
 */
class CartridgePricesController extends Admin\Controllers\ListController{

    public function loadIdBrandItems() {
        $items = \App\Models\Brand::all(array('id','title'))->keyBy('id')->toArray();

        $list = array();
        foreach ($items as $id=>$items) {
            $list[$id] = $items['title'];
        }
        return $list;
    }

    public function loadIdTypeItems() {
        $items = \App\Models\CartridgeType::all(array('id','title'))->keyBy('id')->toArray();

        $list = array();
        foreach ($items as $id=>$items) {
            $list[$id] = $items['title'];
        }
        return $list;
    }

}
