<?php
return [
    'title' => 'Логотипы',
    'model' => 'BrandPicture',
    'fields' => [
        'id' => [
            'title' => 'ID',
            'type' => 'text',
        ],

        'slug' => [
            'title' => 'Ссылка',
            'type' => 'select',
        ],

        'image' => [
            'title' => 'Изображение в списке',
            'type' => 'image',
            'description'=>'Выберите изображение размером 90x70'
        ],


    ],
    'sort' => 'id ASC',

    'actions' => [
        'list' => [
            'show' => ['id','slug']
        ],
        'edit' => [
            'hide' => ['id'],
        ],
        'add' => [
            'hide' => ['id'],
        ],
        'delete' => true
    ]
]
    ;