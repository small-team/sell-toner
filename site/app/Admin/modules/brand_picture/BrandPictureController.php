<?php

/**
 * Created by PhpStorm.
 * User: Серёжа
 * Date: 19.07.2016
 * Time: 15:56
 */
class BrandPictureController extends Admin\Controllers\ListController
{
    public function loadSlugItems() {
        $items = \App\Models\Brand::all(array('id','_slug'))->keyBy('id')->toArray();
        $list = array();
        foreach ($items as $id=>$items) {
            $list[$items['_slug']] = $items['_slug'];
        }
        return $list;
    }
}