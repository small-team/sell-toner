<?php
return [
    'title' => 'Баннеры',
    'model' => 'Slider',
    'sortability' => true,
    'fields' => [
        'id' => [
            'title' => 'Id',
            'type' => 'text',
            'readonly' => true
        ],
        'title' => [
            'title' => 'Заголовок',
            'type' => 'text',
        ],
        'link' => [
            'title' => 'Ссылка',
            'type' => 'text',
        ],
        'description' => [
            'title' => 'Описание',
            'type' => 'text',
        ],
        'image' => [
            'title' => 'Изображение для баннера',
            'type' => 'image',
            'description'=>'Загрузите изображение размером 500х202'
        ],


    ],
    'sort' => '_position ASC',
    'tabs' => [ ],
    'actions' => [
        'list' => [
            'show' => ['id','title']
        ],
        'add' => [
            'hide' => ['id','_position'],
        ],
        'edit' => [
            'hide' => ['id','_position'],
        ],
        'delete' => true
    ]
]
    ;