<?php

return [
    'config'=>[
        'title' => 'Настройки',
        'icon' => 'fa-gears',
        'sub'=>[
            'settings'=>[ 'title'=> 'Настройки сайта','icon' => 'fa-gear'],
            'satellites'=>[ 'title'=> 'Данные сателлита','icon' => 'fa-gear'],
            'acl'=>['title'=> 'Доступ','icon' => 'fa-user'],
//            'slider' => ['title' => 'Баннеры'],
        ]
    ],

    'orders' => [
        'title' => 'Журнал заявок',
    ],
    'news' => [
        'title' => 'Новости и статьи',
    ],
//    'questions' => [
//        'title' => 'Вопрос-ответ',
//        'sub'=>[
//            'questions_on'=>[ 'title'=> 'Одобренные'],
//            'questions_off'=>[ 'title'=> 'На модерации'],
//        ]
//    ],
    'catalog' => [
        'title' => 'Каталоги','icon' => 'fa-tasks',
        'sub'=>[
            'brands'=>[ 'title'=> 'Производители'],
            'cartridge_types'=>[ 'title'=> 'Типи картриджей'],
        ]
    ],
    'prices'=>[
        'title' => 'Прайс-листы','icon' => 'fa-money',
        'sub'=>[
            'cartridge_prices'=>[ 'title'=> 'Картриджи'],
        ]
    ],
    'popular' => [
        'title' => 'Популярные предл.',
    ],
    
    'brand_picture' => [
        'title' => 'Логотипы',
    ],
    
    
     'site_menu' => [
        'title' => 'Меню сайта',
    ],
    

    'meta_tags' => [
        'title' => 'Мета данные',
    ],
    'pages' => [
        'title' => 'Страницы',
    ],


];