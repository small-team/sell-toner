<?php

return [
    'admin' => [
        'prefix' => 'admin',

        /*
         * Blade template prefix, default admin::
         */
        'bladePrefix'                => 'admin::',
    ]
];