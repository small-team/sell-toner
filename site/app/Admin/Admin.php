<?php namespace Admin;

use Illuminate\Config\Repository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Routing\Router as IlluminateRouter;
use Symfony\Component\Finder\Finder;
use Illuminate\Routing\UrlGenerator;

/**
 * Class Admin
 *
 * @package SleepingOwl\Admin
 */
class Admin
{
	/**
	 * Bootstrap filename
	 */
	const BOOTSRAP_FILE = 'bootstrap.php';
	/**
	 * @var Admin
	 */
	public static $instance;

	/**
	 * @var string
	 */
	public $title;
	/**
	 * @var Router
	 */
	public $router;

	/**
	 * @var Finder
	 */
	protected $finder;
	/**
	 * @var string
	 */
	protected $bootstrapDirectory;
	/**
	 * @var Filesystem
	 */
	protected $filesystem;

	/**
	 * @param Finder $finder
	 * @param Repository $config
	 * @param IlluminateRouter $illuminateRouter
	 * @param UrlGenerator $urlGenerator
	 * @param Filesystem $filesystem
	 */
	function __construct(Finder $finder, Repository $config,
						 IlluminateRouter $illuminateRouter, UrlGenerator $urlGenerator, Filesystem $filesystem)
	{
		static::$instance = $this;

		$this->finder = $finder;
		$this->filesystem = $filesystem;

		$this->title = $config->get('admin.title');
		$this->bootstrapDirectory = $config->get('admin.bootstrapDirectory');

		$this->requireModules();

	}

	/**
	 * @return Admin
	 */
	public static function instance()
	{
		if (is_null(static::$instance))
		{
			app('\Admin\Admin');
		}
		return static::$instance;
	}

	/**
	 *
	 */
	protected function requireModules()
	{
		$modules_directory = app_path('Admin').'/modules/';
		if (! $this->filesystem->isDirectory($modules_directory)) return;
		$directories = $this->finder->create()->directories()->in($modules_directory);
		foreach ($directories as $directory)
		{
			$module_directory = $modules_directory.$directory->getRelativePathname().'/';
			$controllers = $this->finder->create()->files()->name('*Controller.php')->in($module_directory);
			foreach ($controllers as $controller)
			{
				$this->filesystem->requireOnce($controller);
			}
		}

	}

	/**
	 *
	 */
	protected function requireBootstrap()
	{
		return false;

		if (! $this->filesystem->isDirectory($this->bootstrapDirectory)) return;
		$files = $this->finder->create()->files()->name('/^[^_].+\.php$/')->in($this->bootstrapDirectory);
		$files->sort(function ($a)
		{
			return $a->getFilename() !== static::BOOTSRAP_FILE;
		});
		foreach ($files as $file)
		{
			$this->filesystem->requireOnce($file);
		}
	}


	/**
	 * Register all admin routes
	 */
	public function registerRoutes($laravelRouter)
	{

		$models = Admin::instance()->models->getAllAliases();

		$this->laravelRouter->group([
				'prefix'    => $this->prefix,
				'before'    => $this->getBeforeFilters(),
				'namespace' => 'SleepingOwl\Admin\Controllers',
		], function () use ($models)
		{
			if (empty($models)) $models = ['__empty_models__'];
			$this->laravelRouter->group([
					'where' => ['model' => implode('|', $models)]
			], function ()
			{
				foreach (static::$modelRoutes as $route)
				{
					$url = $route['url'];
					$action = $route['action'];
					$method = $route['method'];
					$controller = isset($route['controller']) ? $route['controller'] : 'AdminController';
					$this->laravelRouter->$method($url, [
							'as'   => $this->routePrefix . '.table.' . $action,
							'uses' => $controller . '@' . $action
					]);
				}
			});

			$wildcardRoute = $this->laravelRouter->any('{wildcard?}', [
					'as'   => $this->routePrefix . '.wildcard',
					'uses' => 'AdminController@getWildcard'
			])->where('wildcard', '.*');
			$this->setRoutePriority($wildcardRoute, 0);
		});
	}
}