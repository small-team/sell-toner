<?php namespace SmartModel\Exceptions;

use Illuminate\Support\MessageBag;

/**
 * Class ValidationException
 */
class ValidationException extends \Exception
{
	/**
	 * @var MessageBag
	 */
	protected $errors;

	/**
	 * @param MessageBag $errors
	 */
	function __construct(MessageBag $errors)
	{
		$this->errors = $errors;
	}

	/**
	 * @return MessageBag
	 */
	public function getErrors()
	{
		return $this->errors;
	}

}