<?php namespace SmartModel\Traits;

use SmartModel\Exceptions\ValidationException;
use Lang;
use Validator;

/**
 * Class ValidationModelTrait
 *
 * @package SleepingOwl\Admin\Models\Traits
 */
trait ValidationModelTrait
{
	/**
	 * @param $data
	 * @param array $rules
	 * @throws ValidationException
	 * @return bool
	 */

	public $rules = [];
	protected $errors = [];

	protected $messages = [
			'required' => 'Поле обязательно для заполнения.',
			'email' => 'Введите корректный e-mail',
			'url' => 'Введите корректный URL',
			'unique' => 'Подобная запись уже есть в базе'
	];

	public function validate($data, $rules = [], $messages = [])
	{
		if (count($rules) == 0) {
			return true;
		}

		foreach ($messages as $rule=>$message) {
			$this->messages[$rule] = $message;
		}

		$validator = Validator::make($data, $rules, $this->messages, [$this->getKeyName() => $this->getKey()]);
		if ($validator->fails())
		{
			$this->errors = $validator->errors();
			throw new ValidationException($this->errors);
		}
		return true;
	}

	public function errors() {
		return $this->errors;
	}

	/**
	 * @param $rules
	 * @return array
	 */
	protected function mergeValidationRules($rules)
	{
		foreach ($this->getValidationRules() as $field => $rule)
		{
			if (!is_array($rule))
			{
				$rule = explode('|', $rule);
			}
			$rules[$field] = array_merge($rules[$field], $rule);
		}
		return $rules;
	}

	public function getValidationRules()
	{
		return $this->rules;
	}
} 