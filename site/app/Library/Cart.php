<?php

namespace App\library;

class Cart {

    static public function putInfoToView() {
        //      count the number of products and their sum
        $cart_items = \Session::get('order');
        $count_items = 0;
        $sum_items = 0;
        if($cart_items) {
            foreach($cart_items as $value)
            {
                if($value["checked"]!=0) {
                    $count_items += $value["count"];
                    $sum_items += $value["count"] * $value["sum"];
                }

            }
        }
        \View::share(['cart_items'=>$cart_items,'count_items'=>$count_items,'sum_items'=>$sum_items]);
    }

}