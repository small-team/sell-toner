<div class='popup-bg'></div>
<div class='popup'>
    <div class='popup-close'></div>
    <div class="popup-body">
        <div class="popup-body__title title_call">обратный звонок</div>
        <div class="content__title title_1 title_success title_call_true" style="display: none">Ваша заявка успешно отправлена</div>
        <form action="{{route('call_back')}}" method="post" id="call-back-form" class="ajax-form">
            <div class="popup-body-row row">
                <div class="popup-body-column">
                    <input type="text" name="name" data-value="Как к Вам обращаться?" class="popup-body__input" />
                </div>
                <div class="popup-body-column">
                    <input type="text" name="phone" id="phone_call" data-value="Контактный телефон *" class="popup-body__input req number_phone" />
                </div>
            </div>
            <div class="popup-body-row">
                <textarea name="mess" data-value="Комментарии к заявке" class="popup-body__input"></textarea>
            </div>
            <div class="popup-body-row">
                <div id="g-recaptcha-phone"></div>
            </div>
            <div class="popup-body-row">
                <input type="hidden" data-value="call_back"  id="type_call" name="type"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" formaction="{{route('call_back')}}" class="popup-body__btn call_back_button"  @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('CALLBACKSEND')" @endif><span>Отправить</span></button>
            </div>
        </form>
    </div>
</div>