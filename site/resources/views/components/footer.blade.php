<footer class="noprint">
    <div class="footer-top">
        <div class='container'>
            <div class="footer-top-table">
                <div class="footer-top-column">
                    <a href="{{route('home')}}" class="footer__logo"><img src="{{asset('img/footer-logo.png')}}" alt="" /></a>
                </div>
                <div class="footer-top-column">
                    <div class="header-top__phone">
                        @if(\App\Models\Settings::showPhone())
                        {!!$info_company->company_phone_footer!!}
                        @endif
                    </div>
                    <a href="mailto:{!!$info_company->footer_email!!}" class="header-top__email">{!!$info_company->footer_email!!}</a>
                        @if(!empty($info_company->m_viber))
                            <a  href="viber://chat?number={!!$info_company->m_viber!!}" class="header-top__messender m_viber hide_mobile"></a>
                        @endif
                        @if(!empty($info_company->m_whatsapp))
                            <a  href="whatsapp://send?phone={!!$info_company->m_whatsapp!!}" class="header-top__messender m_whatsapp hide_mobile"></a>
                        @endif
                        @if(!empty($info_company->m_telegram))
                            <a  href="tg://resolve?domain={!!$info_company->m_telegram!!}" class="header-top__messender m_telegram hide_mobile"></a>
                        @endif
                        @if(!empty($info_company->m_skype))
                            <a  href="skype:{!!$info_company->m_skype!!}?chat" class="header-top__messender m_skype hide_mobile"></a>
                        @endif
                </div>
                <div class="footer-top-column mobile_messendgers">
                    @if(!empty($info_company->m_viber))
                        <a  href="viber://chat?number={!!$info_company->m_viber!!}" class="header-top__messender m_viber"></a>
                    @endif
                    @if(!empty($info_company->m_whatsapp))
                        <a  href="whatsapp://send?phone={!!$info_company->m_whatsapp!!}" class="header-top__messender m_whatsapp"></a>
                    @endif
                    @if(!empty($info_company->m_telegram))
                        <a  href="tg://resolve?domain={!!$info_company->m_telegram!!}" class="header-top__messender m_telegram"></a>
                    @endif
                    @if(!empty($info_company->m_skype))
                        <a  href="skype:{!!$info_company->m_skype!!}?chat" class="header-top__messender m_skype"></a>
                    @endif
                </div>
                <div class="footer-top-column">
                    {!!$info_company->footer_year!!}
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class='container'>
            {!! $info_company->copyright !!}
        </div>
    </div>
</footer>