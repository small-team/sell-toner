
{{----}}

<table id="print_table">
    <tr>
        <th>Артикул</th>
        <th>Модель принтера</th>
        <th>Цена, руб.</th>
        <th>Количество, шт.</th>
        <th>Сумма, руб.</th>
    </tr>
    @if(isset($cart_orders_item))
        @foreach($cart_orders_item as $key=>$value)
            <tr >
                <td colspan="6">
                    <div id="type-{{$value['0']->typeId}}">{{$key}}</div>
                </td>
            </tr>
            @foreach($value as $item)
                <tr id="tr-{{$item->id}}" >
                    <td>{{$item->articule}}</td>
                    <td>{{$item->brand}} {{$item->cartridge_model}}</td>
                    <td class="price_RUB-{{$item->id}}">{{$item->price_RUB}}</td>
                    <td>
                        <div class="quantity" >
                            <div class=""  id="{{$item->id}}" onselectstart="return false" onmousedown="return false"></div>
                            <input type="text" name="form[]"  value="{{$cart_items[$item->id]["count"]}}" id="{{$item->id}}" class="item_{{$item->id}} number quantity__inp" />
                            <div class=""  id="{{$item->id}}" onselectstart="return false" onmousedown="return false"></div>
                        </div>
                    </td>
                    <td id="order_{{$item->id}}" sum="{{$item->price_RUB}}">{{$cart_items[$item->id]["count"]*$cart_items[$item->id]["sum"]}}</td>
                </tr>
            @endforeach
        @endforeach
    @endif

</table>
