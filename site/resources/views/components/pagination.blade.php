@if ($paginator->lastPage() > 1)
    <div class="content-pagging">
        <ul>
            <li><a href="{{$paginator->url($paginator->currentPage()-1)}}" class="content-pagging__link left"></a></li>

            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                @if ($paginator->currentPage() == $i)
                    <li><a href="" class="content-pagging__link active">{{ $i }}</a></li>
                @else
                    <li><a href="{{ $paginator->url($i) }}" class="content-pagging__link">{{ $i }}</a></li>
                @endif
            @endfor

            @if($paginator->currentPage()==$paginator->lastPage())
                <li><a class="content-pagging__link right"></a></li>
            @else
                <li><a href="{{$paginator->url($paginator->currentPage()+1)}}" class="content-pagging__link right"></a></li>
            @endif
        </ul>
    </div>
@endif
