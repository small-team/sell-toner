<header>
    <div class="container">
        <div class="header-top row">
            <div class="header-top__phone">
                @if(\App\Models\Settings::showPhone())
                    {!!$info_company->company_phone!!}
                @endif
            </div>
            <a  href="mailto:{!!$info_company->company_email!!}" class="header-top__email">{!!$info_company->company_email!!}</a>
            @if(!empty($info_company->m_viber))
                <a  href="viber://chat?number={!!$info_company->m_viber!!}" class="header-top__messender m_viber"></a>
            @endif
            @if(!empty($info_company->m_whatsapp))
                <a  href="whatsapp://send?phone={!!$info_company->m_whatsapp!!}" class="header-top__messender m_whatsapp"></a>
            @endif
            @if(!empty($info_company->m_telegram))
                <a  href="tg://resolve?domain={!!$info_company->m_telegram!!}" class="header-top__messender m_telegram"></a>
            @endif
            @if(!empty($info_company->m_skype))
                <a  href="skype:{!!$info_company->m_skype!!}?chat" class="header-top__messender m_skype"></a>
            @endif
            <a  href="" class="header-top__callback noprint"><span>Заказать обратный звонок</span></a>
        </div>
        <div class="header-bottom row">
            <a id="logo" href="{{route('home')}}" class="header__logo"><img src="{{asset('img/logo.png')}}" alt="" /></a>
            <a id="logo_print" href="{{route('home')}}" class="header__logo" style="display: none"><img src="{{asset('img/logo__black.png')}}" alt="" /></a>
            <div class="header__menuicon">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="header-search noprint">
                <form action="{{route('search')}}" method="get">
                    <input type="text" name="key" value="@if(!empty($key_search)){{$key_search}}@endif" data-value="Например, CE278A или номер 278" class="header-search__input" />
                    <button type="submit" @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('SEARCH')" @endif formaction="{{route('search')}}" class="header-search__btn"></button>
                </form>
            </div>
            <div class="noprint cart_top">
                @if(count($cart_items))
                    <a href="{{route('cart')}}" class="header-cart active " id="cart_full">
                        <div class="header-cart-values"><strong class="header-cart__prod_span">{{$count_items}}</strong> шт., <strong class="header-cart__summ_span">{{$sum_items}} руб.</strong></div>
                    </a>
                    <div class="header-cart" id="cart_empty" style="display: none">
                        <div class="header-cart-values">Ваша корзина пуста</div>
                    </div>
                @else
                    <a href="{{route('cart')}}" class="header-cart active " id="cart_full" style="display: none">
                        <div class="header-cart-values"><strong class="header-cart__prod_span">
                            </strong> шт., <strong class="header-cart__summ_span"> руб.</strong></div>
                    </a>
                    <div class="header-cart" id="cart_empty">
                        <div class="header-cart-values">Ваша корзина пуста</div>
                    </div>
                @endif
            </div>
        </div>
        <div class="header-menu">
            <ul class="header-menu-list">
                <li><a href="{{route('home')}}" class="header-menu-list__link"><span>Главная</span></a></li>
                <li><a href="" class="header-menu-list__link no-click" c><span>Прайс-листы</span></a>
                    <ul class="header-menu-sublist">
                        <li><a href="{{route('exel_download')}}" class="header-menu-sublist__link" @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('DOWNLOADALLPRICE')" @endif><span>Скачать общий прайс-лист</span></a></li>
                        @if(count($brands))
                            @foreach($brands as $item_brand)
                                <li><a href="{{route('price_brand',$item_brand->_slug)}}" class="header-menu-sublist__link"><span>Скупка картриджей {{$item_brand->title}}</span></a></li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                <li><a  href="{{route('home')}}#mainruls" class="header-menu-list__link"><span>Правила работы</span></a></li>
                <li><a  href="{{route('home')}}#mainpopular" class="header-menu-list__link"><span>Популярные предложения</span></a></li>
                <li><a href="{{route('news')}}" class="header-menu-list__link"><span>Новости</span></a></li>
            </ul>
        </div>
    </div>
</header>