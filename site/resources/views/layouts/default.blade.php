<!DOCTYPE html>
<html class="no-js">
	<head>
		{{\App\Models\MetaTag::printMetaData()}}
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

		<link href="{{ elixir('assets/css/all.css') }}" rel="stylesheet">

		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		@if(isset($show_paging_header))
			<link rel="canonical" href="{{$current_url}}"/>
		@endif
{{--		<script src="https://www.google.com/recaptcha/api.js?render={{env('reCAPTCHA_KEY')}}"></script>--}}
{{--		<script src="https://www.google.com/recaptcha/api.js" async defer></script>--}}
		<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
		<script type="text/javascript">
			var CaptchaCallback = function() {
				grecaptcha_id = grecaptcha.render('g-recaptcha', {'sitekey' : '{{env('reCAPTCHA_KEY')}}'});
				grecaptcha_phone_id = grecaptcha.render('g-recaptcha-phone', {'sitekey' : '{{env('reCAPTCHA_KEY')}}'});
			};
		</script>
	</head>

	<body class="mainpage">
		<div class="wrapper">

		{{--header--}}
			@include('components.header')
		{{--header--}}

			@yield('content')

		{{--footer--}}
			@include('components.footer')
		{{--footer--}}

		</div>

		{{--call_back--}}
		@include('components.call_back')
		{{--call_back--}}


		<!--[if IE]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0-beta.1/jquery-ui.min.js"></script>

		<script src="{{ elixir('assets/js/all.js') }}"></script>

		@if(!empty($info_company->metrika))
			@if(isset($_SERVER['HTTP_X_REAL_IP']))
				<script type="text/javascript">
					var yaParams = {ip_adress: "<?php echo $_SERVER['HTTP_X_REAL_IP'];?>"};
				</script>
			@endif

			{!!$info_company->metrika!!}
		@endif

	@yield('additional-js')

	</body>
</html>
