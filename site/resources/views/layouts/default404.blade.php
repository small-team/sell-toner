<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	{{--meta--}}
	{{\App\Models\MetaTag::printMetaData()}}

	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link type="text/css" rel="stylesheet" href="/css/style.css">
	<link rel="shortcut icon" href="/favicon.ico">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->
	<meta name="viewport" content="width=1000"/>
	<!--[if IE]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/slick.min.js"></script>
    <script type="text/javascript" src="/js/script.js"></script>
</head>
<body>
	<div class="wrapper">
	    {{--header--}}
		<header>
        			<div class="container">
        				<div class="header-table">
        					<div class="header-element header-element__logo">
        						<a href="{{URL::route('home')}}"><img src="/img/logo.png" alt="" /></a>
        					</div>


        				</div>
        			</div>
        </header>
		{{--topblock--}}

		{{--content--}}
		<div class="content">
        			<div class="container">
        				<div class="content-body row">
        					{{--content-center--}}
        					@yield('content-center')
                        </div>
                    </div>
        </div>
    </div>
</body>
</html>
