@if(isset($code))<p>Номера заказа : {{$code}}</p>@endif
@if(isset($date))<p>Дата и время обращения : {{$date}}</p>@endif
@if(isset($name))<p>Имя клиента : {{$name}}</p>@endif
@if(isset($phone))<p>Контактный телефон : {{$phone}}</p>@endif
@if(isset($city))<p>Город : {{$city}}</p>@endif
@if(isset($question))<p>Вопрос клиента : {{$question}}</p>@endif
@if(isset($email))<p>E-mail : {{$email}}</p>@endif
@if(isset($comment))<p>Что вы хотите нам предложить : {{$comment}}</p>@endif
@if(isset($site_url))<p>Адрес сайта : {{$site_url}}</p>@endif
@if(isset($type))<p>Тип заявки : {{$type}}</p>@endif
<br>
@if(!empty($photo_links))
<table >
    <thead>
    <tr>
        <td>Ссылки на прикрепленные фото </td>
    </tr>
    </thead>
    @if(isset($photo_links) && $photo_links)
        @foreach($photo_links as $key=>$photo)
                    <tr>
                        <td>{{$photo['link']}}</td>
                    </tr>
        @endforeach
    @endif
</table>
@else
<table>
    <thead>
        <tr>Нет прикреплённых фото</tr>
    </thead>
</table>
@endif