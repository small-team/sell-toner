<p>Номера заказа : @if(isset($code)){{$code}}@endif</p>
<p>Дата и время обращения : @if(isset($date)){{$date}}@endif</p>
<p>Имя клиента : @if(isset($name)){{$name}}@endif</p>
<p>Контактный телефон : @if(isset($phone)){{$phone}}@endif</p>
<p>Город : @if(isset($city)){{$city}}@endif</p>
<p>E-mail : @if(isset($email)){{$email}}@endif</p>
<p>Что вы хотите нам предложить : @if(isset($comment)){{$comment}}@endif</p>
<p>Адрес сайта : @if(isset($site_url)){{$site_url}}@endif</p>
<p>Тип заявки : @if(isset($type)){{$type}}@endif</p>
@if(!empty($order_info))
<table >
    <thead>
    <tr>
            <td>Артикул</td>
            <td>Модель</td>
            <td>Количество</td>
            <td>Цена</td>
            <td>Сумма</td>
            <td>Стоимость заказа, руб.</td>
            <td>Количество в заказе, шт.</td>
    </tr>
    </thead>
    @if(isset($order_info) && $order_info)
        @foreach($order_info as $key=>$product)
                    <tr>
                        <td>{{$product['articule']}}</td>
                        <td>{{$product['model']}}</td>
                        <td>{{$product['quantity']}}</td>
                        <td>{{$product['price']}}</td>
                        <td>{{$product['sum']}}</td>
                        @if($key == 0)
                        <td>@if(isset($sum_items)){{$sum_items}}@endif</td>
                        <td>@if(isset($count_items)){{$count_items}}@endif</td>
                        @endif
                    </tr>
        @endforeach
    @endif
</table>
@else
<table>
    <thead>
        <tr>Товаров нет</tr>
    </thead>
</table>
@endif
<br>
@if(!empty($photo_links))
<table >
    <thead>
    <tr>
            <td>Ссылки на прикрепленные фото </td>
    </tr>
    </thead>
    @if(isset($photo_links) && $photo_links)
        @foreach($photo_links as $key=>$photo)
                    <tr>
                        <td>{{$photo['link']}}</td>
                    </tr>
        @endforeach
    @endif
</table>
@else
<table>
    <thead>
        <tr>Нет прикреплённых фото</tr>
    </thead>
</table>
@endif
