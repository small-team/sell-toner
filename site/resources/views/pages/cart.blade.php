@extends('layouts.default')
@section('content')
    @if(!isset($cart_orders_item))
        @if(isset($send) && $send)
            <div class='container cart_empty_text'>
                <div class="content-breadcrumbs noprint">
                    {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
                </div>
                <div class="content__title title_1 title_success">Ваша заявка @if(isset($code)) № {{$code}}  от {{$date}} @endifуспешно отправлена</div>
            </div>
        @else
            <div class='container cart_empty_text'>
                <div class="content-breadcrumbs noprint">
                    {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
                </div>
                <div class="content__title title_1">Корзина пуста</div>
            </div>
        @endif

    @else
    <div class="content">
        <div class='container'>
            <div class="content-breadcrumbs noprint">
                {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
            </div>
            <div class="content__title title_1 noprint">оформление заявки <br />на продажу картриджей</div>
            <div class="content__title_print">Заявка от {{date("d.m.Y")}}</div>


            <div class="content-filter " >
                @if(isset($count_items))
                    <div class="content-filter-sector " >Выбрано товаров: <strong  id="cart_count">{{$count_items}}</strong> шт.</div>
                    <div class="content-filter-sector "  >На сумму: <strong id="cart_sum">{{$sum_items}}</strong> руб.</div>
                @else
                    <div class="content-filter-sector " >Выбрано товаров: <strong>0 шт.</strong></div>
                    <div class="content-filter-sector " >На сумму: <strong>0 руб.</strong></div>
                @endif
            </div>

            <div class="filter_print_div" >
                @if(isset($count_items))
                    <div class="filter_print" style="margin-right: 8px;" >Выбрано товаров: <strong  id="cart_count">{{$count_items}}</strong> шт.</div>
                    <div class="filter_print "  >На сумму: <strong id="cart_sum">{{$sum_items}}</strong> руб.</div>
                @else
                    <div class="filter_print" style="margin-right: 8px;">Выбрано товаров: <strong>0 шт.</strong></div>
                    <div class="filter_print" >На сумму: <strong>0 руб.</strong></div>
                @endif
            </div>


            <div class="content-table " id="print_table">
                <table>
                    <tr>
                        <th>Артикул</th>
                        <th>модель принтера</th>
                        <th>цена, руб.</th>
                        <th>количество, шт.</th>
                        <th>сумма, руб.</th>
                        <th class="noprint">удалить</th>
                    </tr>
                    @if(isset($cart_orders_item))
                        @foreach($cart_orders_item as $key=>$value)
                            <tr>
                                <td colspan="6">
                                    <div id="type-{{$value['0']->typeId}}" class="content-table__subheader">{{$key}}</div>
                                </td>
                            </tr>
                            @foreach($value as $item)
                                <tr id="tr-{{$item->id}}">
                                    <td >{{$item->articule}}</td>
                                    <td>{{$item->brand}} {{$item->cartridge_model}}</td>
                                    <td class="price_RUB-{{$item->id}}">{{$item->price_RUB}}</td>
                                    <td >
                                        <div class="quantity">
                                            <div class="quantity__btn dwn noprint"  id="{{$item->id}}" onselectstart="return false" onmousedown="return false"></div>
                                            <input type="text" name="form[]"  value="{{$cart_items[$item->id]["count"]}}" id="{{$item->id}}" class="item_{{$item->id}} number quantity__inp " />
                                            <div class="quantity__btn up noprint"  id="{{$item->id}}" onselectstart="return false" onmousedown="return false"></div>
                                        </div>
                                    </td>
                                    <td id="order_{{$item->id}}" sum="{{$item->price_RUB}}">{{$cart_items[$item->id]["count"]*$cart_items[$item->id]["sum"]}}</td>
                                    <td class="noprint"><a class="content-table__dell type-{{$item->typeId}}" data-filter="{{$item->typeId}}" data-href="{{URL::route('cart_item_delete')}}" id="{{$item->id}}"></a></td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endif

                </table>
            </div>
            <div class="content-form noprint">
                <div class="content-form__title title_2">ваши контактные данные<br />для заявки</div>
                <form action="{{route('save_order')}}" enctype="multipart/form-data" method='POST'>
{{--                    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">--}}
                    {{ HiddenCaptcha::render() }}
                    <div class="popup-body-row row" >
                        <div class="popup-body-column ">
                            <input type="text" name="name" data-value="Как к Вам обращаться?" class="popup-body__input" />
                        </div>
                        <div class="popup-body-column ">
                            <input type="text"  name="phone" data-value="Контактный телефон" class="number_phone popup-body__input" />
                        </div>
                        <div class="popup-body-column ">
                            <input type="text" name="email" data-value="Электронная почта" class="popup-body__input req email" />
                        </div>
                        <div class="popup-body-column ">
                            <div class="select content-filter__select">
                                <div class="select-title">
                                    <div class="select-title__arrow"></div>
                                    <div class="select-title__value">Екатеринбург</div>
                                </div>
                                <div class="select-options">
                                    <div class="select-options-inside">
                                        <div class="select-options__value">Екатеринбург</div>
                                        <div class="select-options__value">Свердловская область и Урал</div>
                                        <div class="select-options__value">Другой город</div>
                                    </div>
                                    <input type="hidden" class="req" name="place" value="Екатеринбург">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="popup-body-row">
                        <textarea name="mess" data-value="Комментарии к заявке" class="popup-body__input"></textarea>
                    </div>
                    <div class="popup-body-row row noprint">
                        <div class="popup-body-column">&nbsp;</div>
                        <div class="popup-body-column">&nbsp;</div>
                        <div class="popup-body-column-recaptcha">
                            <div id="g-recaptcha"></div>
                        </div>
                    </div>
                    <div class="popup-body-row row noprint">
                        <div class="popup-body-column">
                            <div class="popup-body__file">
                                <span>Загрузить фотографии</span>
                            </div>
                            <input type="file" name="images[]" multiple class="popup-body__hiddenfile" />
                        </div>
                        <div class="popup-body-column">
                            <button type="button"  data-href="{{URL::route('clean_cart')}}" class="popup-body__clinbtn clin" @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('KORZINAFORMCLEAR')" @endif><span>очистить</span></button>
                        </div>
                        <div class="popup-body-column">
                            <button type="button" id="print_cart" class="popup-body__printbtn"  @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('KORZINAFORMPRINT')" @endif ><span>распечатать заявку</span></button>
                        </div>
                        <div class="popup-body-column noprint">
                            <input type="hidden" data-value="cart_request"  name="type"/>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" formaction="{{route('save_order')}}" class="popup-body__btn" @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('KORZINAFORMOTPR')" @endif><span>Отправить заявку</span></button>
                        </div>
                    </div>
                    <div class="popup-body-row last noprint">
                        {{--<div class="popup-body-req">--}}
                            {{--<span class="star">*</span> Обязательные поля--}}
                        {{--</div>--}}
                    </div>
                </form>
            </div>
            <div class="print_order_form">
                <p class="print_order_form_text">Ваши контактные данные</p>
                <table id="print_form">
                    <tr>
                        <td class="col1">Имя:</td>
                        <td class="print_name"></td>
                    </tr>
                    <tr>
                        <td class="col1">Контактный телефон:</td>
                        <td  class="print_phone"></td>
                    </tr>
                    <tr>
                        <td class="col1">Электронная почта:</td>
                        <td  class="print_email"></td>
                    </tr>
                    <tr>
                        <td class="col1">Ваш город:</td>
                        <td  class="print_city"></td>
                    </tr>
                    <tr>
                        <td class="col1">Комментарий к заявке</td>
                        <td  class="print_comment"></td>
                    </tr>

                </table>
            </div>

        </div>
    </div>
    <div class="mainsteps mainsteps__inside noprint">
        <div class='container'>
            <div class="mainsteps__title title_3">после отправки заявки</div>
            <div class="mainsteps-row row">
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/01.png" alt="" /></div>
                        <div class="mainsteps-item__text">Вы оставляете заявку или звоните нам</div>
                    </div>
                </div>
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/02.png" alt="" /></div>
                        <div class="mainsteps-item__text">Согласование условий и встречи</div>
                    </div>
                </div>
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/03.png" alt="" /></div>
                        <div class="mainsteps-item__text">Проверка состояния, фактическая оценка</div>
                    </div>
                </div>
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/04.png" alt="" /></div>
                        <div class="mainsteps-item__text">Оплата наличными или на карту</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@stop

@section('additional-js')
    @if(!isset($cart_orders_item))
        @if(isset($send) && $send)
            <script>
                ym(38574200,'reachGoal','SENDFORMUSPEHKORZINA');
            </script>
        @endif
    @endif
@stop