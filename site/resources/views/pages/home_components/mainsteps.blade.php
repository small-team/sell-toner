<div class="mainsteps">
    <div class='container'>
        <div class="mainsteps__title title_1">4 простых шага нашего<br />сотрудничества</div>
        <div class="mainsteps-row row">
            <div class="mainsteps-column">
                <div class="mainsteps-item">
                    <div class="mainsteps-item__icon"><img src="img/steps/01.png" alt="" /></div>
                    <div class="mainsteps-item__text">Вы оставляете заявку или звоните нам</div>
                </div>
            </div>
            <div class="mainsteps-column">
                <div class="mainsteps-item">
                    <div class="mainsteps-item__icon"><img src="img/steps/02.png" alt="" /></div>
                    <div class="mainsteps-item__text">Согласование условий и встречи</div>
                </div>
            </div>
            <div class="mainsteps-column">
                <div class="mainsteps-item">
                    <div class="mainsteps-item__icon"><img src="img/steps/03.png" alt="" /></div>
                    <div class="mainsteps-item__text">Проверка состояния, фактическая оценка</div>
                </div>
            </div>
            <div class="mainsteps-column">
                <div class="mainsteps-item">
                    <div class="mainsteps-item__icon"><img src="img/steps/04.png" alt="" /></div>
                    <div class="mainsteps-item__text">Оплата наличными или на карту</div>
                </div>
            </div>
        </div>
    </div>
</div>