<a name="mainruls"></a>
<div class="mainruls blockbg" >
    <div class='container'>
        <div class="mainruls__title title_3">правила работы</div>
        <div class="mainruls-block row">
            <div class="mainruls-column">
                <div class="mainruls-item">
                    <div class="mainruls-item__icon">1</div>
                    <div class="mainruls-item__text"><strong>Не скупаем</strong> Б/У картриджи. Все картриджи должны быть в заводской упаковке.</div>
                </div>
            </div>
            <div class="mainruls-column">
                <div class="mainruls-item">
                    <div class="mainruls-item__icon">2</div>
                    <div class="mainruls-item__text">Покупаем только оригинальные картриджи. Восстановленные, совместимые подделки - <br /><strong>не скупаем</strong>.</div>
                </div>
            </div>
            <div class="mainruls-column">
                <div class="mainruls-item">
                    <div class="mainruls-item__icon">3</div>
                    <div class="mainruls-item__text"><strong>Срок годности</strong> картриджей: струйные не менее 6 мес., лазерные - не менее 3 лет.</div>
                </div>
            </div>
            <div class="mainruls-column">
                <div class="mainruls-item">
                    <div class="mainruls-item__icon">4</div>
                    <div class="mainruls-item__text">Оплата товара (в т.ч. и отправленного с регионов) производится только после проверки. Весь товар оплачивается по факту. <br /> <strong>Предоплаты нет</strong>.</div>
                </div>
            </div>
        </div>
    </div>
</div>