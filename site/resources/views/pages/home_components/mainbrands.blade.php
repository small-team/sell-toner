<div class="mainbrands">
    <div class='container'>
        <div class="mainbrands__title title_1">Скупаем картриджи <br />следующих брендов</div>
        <div class="mainbrands-row">

        @if(count($brands_picture))
                @foreach($brands_picture as $brand)
                    <div class="mainbrands-item greyimg">
                            <a href="{{route('price_brand',$brand->slug)}}"><img src="{{ $brand->image_link}}" alt="" /></a>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>