@if(count($populars))
    <a name="mainpopular"></a>
    <div class="mainpopular">
        <div class='container'>
            <div class="mainpopular__title title_3">популярные предложения</div>
            <div class="mainpopular-row row">
                @foreach($populars as $popular)
                    <div class="mainpopular-column">
                        <div class="mainpopular-item">
                            <div class="mainpopular-item__image">
                                <a href="{{$popular->slug}}"><img src="{{$popular->image_url}}" alt="" /></a>
                            </div>
                            <a href="" style="word-break: break-all;" class="mainpopular-item__title">{{str_limit($popular->title, 16)}}</a>
                            <div class="mainpopular-item__price">{{$popular->price}} руб</div>
                            <a href="{{$popular->slug}}" class="mainpopular-item__btn">продать</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
