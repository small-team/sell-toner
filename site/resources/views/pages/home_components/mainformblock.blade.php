<div class="mainformblock">
    <div class='container'>
        <div class="mainformblock-block">
            <div class="mainformblock-block-cell">
                <div class="mainformblock-form">
                    @if(!empty(Session::get('send')) && Session::get('send'))
                        <div class="mainformblock-form__title title_2 title_success">Ваша заявка № {{Session::get('code')}} от {{Session::get('date')}} успешно отправлена</div>
                    @else
                        <div class="mainformblock-form__title title_2">запрос на оценку и продажу<br />картриджей</div>
                    @endif
                    <form action="{{route('store_request')}}" method="post" enctype="multipart/form-data">
{{--                        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">--}}
                        {{ HiddenCaptcha::render() }}
                        <div class="popup-body-row row">
                            <div class="popup-body-column">
                                <input type="text" name="name" data-value="Как к Вам обращаться?" class="popup-body__input" />
                            </div>
                            <div class="popup-body-column">
                                <input type="text" name="phone" data-value="Контактный телефон" placeholder="Контактный телефон" class="popup-body__input" />
                            </div>
                        </div>
                        <div class="popup-body-row row">
                            <div class="popup-body-column">
                                <input type="text" name="email" data-value="Электронная почта" class="popup-body__input req email" />
                            </div>
                            <div class="popup-body-column">
                                <div class="select content-filter__select">
                                    <div class="select-title">
                                        <div class="select-title__arrow"></div>
                                        <div class="select-title__value">Екатеринбург</div>
                                    </div>
                                    <div class="select-options">
                                        <div class="select-options-inside">
                                            <div class="select-options__value">Екатеринбург</div>
                                            <div class="select-options__value">Свердловская область и Урал</div>
                                            <div class="select-options__value">Другой город</div>
                                        </div>
                                        <input type="hidden" class="req" name="place" value="Екатеринбург">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-body-row">
                            <textarea name="mess" data-value="Комментарии к заявке" class="popup-body__input"></textarea>
                        </div>
                        <div class="popup-body-row row">
                            <div class="popup-body-column-recaptcha">
                                <div id="g-recaptcha"></div>
                            </div>
                        </div>
                        <div class="popup-body-row row">
                            <div class="popup-body-column">
                                <div class="popup-body__file">
                                    <span>Загрузить фотографии</span>
                                </div>
                                <input type="file" name="images[]" class="popup-body__hiddenfile" multiple/>
                            </div>
                            <div class="popup-body-column">
                                <input type="hidden" data-value="quick_request"  name="type"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" formaction="{{route('store_request')}}" @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('GLAVFORMOTPR')" @endif class="popup-body__btn"><span>Отправить заявку</span></button>
                            </div>
                        </div>
                        <div class="popup-body-row last">
                            {{--<div class="popup-body-req">--}}
                                {{--<span class="star">*</span> Обязательные поля--}}
                            {{--</div>--}}
                        </div>
                    </form>
                </div>
            </div>

            <div class="mainformblock-block-cell">
                <div class="mainformblock-info">
                    <div class="mainformblock-info-item">
                        <div class="mainformblock-info-item__img"><img src="img/maininfoblock/01.png" alt="" /></div>
                        <div class="mainformblock-info-item__text">Скупаем только новые картриджи</div>
                    </div>
                    <div class="mainformblock-info-item">
                        <div class="mainformblock-info-item__img"><img src="img/maininfoblock/02.png" alt="" /></div>
                        <div class="mainformblock-info-item__text">Не покупаем Б/У, восстановленные совместимые картриджи</div>
                    </div>
                    <div class="mainformblock-info-item">
                        <div class="mainformblock-info-item__img"><img src="img/maininfoblock/03.png" alt="" /></div>
                        <div class="mainformblock-info-item__text">Принимаем заявки по всей России. Сотрудничаем с региональными дилерами.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('additional-js')
    @if(!empty(Session::get('send')) && Session::get('send'))
        <script>
            ym(38574200,'reachGoal','SENDFORMUSPEHGLAV');
        </script>
    @endif
@stop