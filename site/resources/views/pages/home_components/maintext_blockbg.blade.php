@if(count($info_company->main_page_content))
    <div class="maintext blockbg">
        <div class='container'>
            <div class="maintext__title title_3">покупка картриджей для принтеров</div>

            <div class="maintext-block">
                {!! html_entity_decode($info_company->main_page_content) !!}
            </div>
        </div>
    </div>
@endif
