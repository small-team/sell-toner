@extends('layouts.default')

@section('content')
    <div class="content">
        <div class='container'>
            <div class="content-breadcrumbs">
                {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
            </div>
            <div class="content__title title_3">поиск</div>
            <div class="content-filter">
                @if(isset($count_items))
                    <div class="content-filter-sector">Выбрано товаров: <strong  id="cart_count">{{$count_items}}</strong> шт.</div>
                    <div class="content-filter-sector" >На сумму: <strong id="cart_sum">{{$sum_items}}</strong> руб.</div>
                @else
                    <div class="content-filter-sector">Выбрано товаров: <strong>0 шт.</strong></div>
                    <div class="content-filter-sector">На сумму: <strong>0 руб.</strong></div>
                @endif
            </div>
            <div class="content-table">
                <table>
                    <tr>
                        <th>Артикул</th>
                        <th>модель принтера</th>
                        <th>цена, руб.</th>
                        <th>количество, шт.</th>
                        <th>сумма, руб.</th>
                    </tr>
                    @if(count($cartridge_price_item))

                        @foreach($cartridge_price_item as $item)

                                <tr id="tr-{{$item->id}}">
                                    <td>{{$item->articule}}</td>
                                    <td>{{$item->brand}} {{$item->cartridge_model}}</td>
                                    <td class="price_RUB-{{$item->id}}">{{$item->price_RUB}}</td>
                                    <td>
                                        <div class="quantity">
                                            <div class="quantity__btn dwn"  id="{{$item->id}}" onselectstart="return false" onmousedown="return false"></div>
                                            @if(isset($cart_items[$item->id]))
                                                <input type="text" name="form[]"  value="{{$cart_items[$item->id]["count"]}}" id="{{$item->id}}" class="item_{{$item->id}} number quantity__inp" />
                                            @else
                                                <input type="text" name="form[]"  value="0" id="{{$item->id}}" class="item_{{$item->id}} number quantity__inp" />
                                            @endif

                                            <div class="quantity__btn up"  id="{{$item->id}}" onselectstart="return false" onmousedown="return false"></div>
                                        </div>
                                    </td>
                                    @if(isset($cart_items[$item->id]))
                                        <td id="order_{{$item->id}}" sum="{{$item->price_RUB}}">{{$cart_items[$item->id]["count"]*$cart_items[$item->id]["sum"]}}</td>
                                    @else
                                        <td id="order_{{$item->id}}" sum="{{$item->price_RUB}}">{{$item->price_RUB}}</td>
                                    @endif
                                </tr>
                            @endforeach
                    @endif

                </table>
            </div>
            <div class="content-form">
                <div class="content-form__title title_2">ваши контактные данные<br />для заявки</div>
                <form action="{{route('save_order')}}" enctype="multipart/form-data" method='POST'>
{{--                    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">--}}
                    {{ HiddenCaptcha::render() }}
                    <div class="popup-body-row row">
                        <div class="popup-body-column">
                            <input type="text" name="name" data-value="Как к Вам обращаться?" class="popup-body__input" />
                        </div>
                        <div class="popup-body-column">
                            <input type="text"  name="phone" data-value="Контактный телефон" class="number_phone popup-body__input" />
                        </div>
                        <div class="popup-body-column">
                            <input type="text" name="email" data-value="Электронная почта" class="popup-body__input req email" />
                        </div>
                        <div class="popup-body-column">
                            <div class="select content-filter__select">
                                <div class="select-title">
                                    <div class="select-title__arrow"></div>
                                    <div class="select-title__value">Екатеринбург</div>
                                </div>
                                <div class="select-options">
                                    <div class="select-options-inside">
                                        <div class="select-options__value">Екатеринбург</div>
                                        <div class="select-options__value">Свердловская область и Урал</div>
                                        <div class="select-options__value">Другой город</div>
                                    </div>
                                    <input type="hidden" class="req" name="place" value="Екатеринбург">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="popup-body-row">
                        <textarea name="mess" data-value="Комментарии к заявке" class="popup-body__input"></textarea>
                    </div>
                    <div class="popup-body-row row">
                        <div class="popup-body-column">&nbsp;</div>
                        <div class="popup-body-column">&nbsp;</div>
                        <div class="popup-body-column-recaptcha">
                            <div id="g-recaptcha"></div>
                        </div>
                    </div>
                    <div class="popup-body-row row">
                        <div class="popup-body-column">
                            <div class="popup-body__file">
                                <span>Загрузить фотографии</span>
                            </div>
                            <input type="file" name="images[]" multiple class="popup-body__hiddenfile" />
                        </div>
                        <div class="popup-body-column">
                            <button type="button"  data-href="{{URL::route('clean_cart')}}" class="popup-body__clinbtn clin"><span>очистить</span></button>
                        </div>
                        <div class="popup-body-column">
                            <a href="{{route('cart')}}"><button type="button" class="popup-body__printbtn"><span>перейти в корзину</span></button></a>
                        </div>
                        <div class="popup-body-column">
                            <input type="hidden" data-value="cart_request"  name="type"/>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" formaction="{{route('save_order')}}" class="popup-body__btn"  ><span>Отправить заявку</span></button>
                        </div>
                    </div>
                    <div class="popup-body-row last">
                        {{--<div class="popup-body-req">--}}
                            {{--<span class="star">*</span> Обязательные поля--}}
                        {{--</div>--}}
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div class="mainsteps mainsteps__inside">
        <div class='container'>
            <div class="mainsteps__title title_3">после отправки заявки</div>
            <div class="mainsteps-row row">
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/01.png" alt="" /></div>
                        <div class="mainsteps-item__text">Вы оставляете заявку или звоните нам</div>
                    </div>
                </div>
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/02.png" alt="" /></div>
                        <div class="mainsteps-item__text">Согласование условий и встречи</div>
                    </div>
                </div>
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/03.png" alt="" /></div>
                        <div class="mainsteps-item__text">Проверка состояния, фактическая оценка</div>
                    </div>
                </div>
                <div class="mainsteps-column">
                    <div class="mainsteps-item">
                        <div class="mainsteps-item__icon"><img src="img/steps/04.png" alt="" /></div>
                        <div class="mainsteps-item__text">Оплата наличными или на карту</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop