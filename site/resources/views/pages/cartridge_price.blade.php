@extends('layouts.default')
@section('content')
    <div class="content">
        <div class='container'>
            <div class="content-breadcrumbs">
                {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
            </div>
            <div class="content__title title_3">скупка картриджей {{$cartridge_brand->title}}</div>
            {{--<div class="content-text">--}}
                {{--<p>Внимание покупателям! Наша компания осуществляет покупку только новых картриджей НР (не Б/У). Конечная стоимость зависит от состояния упаковки, срока годности, количества.</p>--}}
            {{--</div>--}}
            <div class="content-filter">
                <div class="content-filter-sector" style=" width: 190px;"></div>
                <div class="content-filter-sector">Отображать на странице: <a href="{{route('price_brand',['slug'=>$cartridge_brand->_slug,'pagination'=>10])}}" class="content-filter__control">10</a> <a href="{{route('price_brand',['slug'=>$cartridge_brand->_slug,'pagination'=>50])}}" class="content-filter__control">50</a> <a href="{{route('price_brand',['slug'=>$cartridge_brand->_slug])}}" class="content-filter__control active">Все</a></div>
                <div class="content-filter-sector"><a href="{{URL::route('exel_download',$cartridge_brand->_slug)}}" class="content__pricedwn" @if(!env('TEST_SITE', false)) onclick="yaCounter38574200.reachGoal('DOWNLOADPRICE')" @endif><span>Скачать прайс-лист {{$cartridge_brand->title}}</span></a></div>
            </div>
            <ul class="price-shortlinks">
                <!--noindex-->
                @if(!empty($cartridge_price_item) && count($cartridge_price_item)>1)
                    @foreach(array_keys($cartridge_price_item) as $i=>$key)
                        <li>
                            <a class="price-shortlink" href="#type-{{$i+1}}" rel="nofollow">{{$key}}</a>
                        </li>
                    @endforeach
                <!--/noindex-->
                @endif
            </ul>
            <div class="content-table">
                <table>
                    <tr>
                        <th>Артикул</th>
                        <th>модель принтера</th>
                        <th>цена, руб.</th>
                        <th>количество, шт.</th>
                        <th>сумма, руб.</th>
                        {{--<th>в корзину</th>--}}
                    </tr>
                    @if(!empty($cartridge_price_item))
                        <?php $i = 1; ?>
                        @foreach($cartridge_price_item as $key=>$value)
                            <tr>
                                <td colspan="6">
                                    <div class="content-table__subheader" id="type-{{$i++}}">{{$key}}</div>
                                </td>
                            </tr>
                            @foreach($value as $item)
                                <tr>
                                    <td>{{$item->articule}}</td>
                                    <td class="model">
                                        <div>
                                            {{$item->cartridge_model}}
                                        </div>
                                    </td>
                                    <td class="price_RUB-{{$item->id}}">{{$item->price_RUB}}</td>
                                    <td>
                                        <div class="quantity">
                                            <div class="quantity__btn dwn" id="{{$item->id}}" onselectstart="return false" onmousedown="return false"></div>
                                            @if(isset($cart_items[$item->id]))
                                                <input type="text" name="form[]" value="{{$cart_items[$item->id]["count"]}}" id="{{$item->id}}" class="number quantity__inp item_{{$item->id}}" />
                                            @else
                                                <input type="text" name="form[]" value="0" id="{{$item->id}}" class="number quantity__inp item_{{$item->id}}" />
                                            @endif
                                            <div class="quantity__btn up" id="{{$item->id}}" onselectstart="return false" onmousedown="return false" ></div>
                                        </div>
                                    </td>
                                    @if(isset($cart_items[$item->id]))
                                        <td id="order_{{$item->id}}" sum="{{$item->price_RUB}}">{{$cart_items[$item->id]["count"]*$cart_items[$item->id]["sum"]}}</td>
                                    @else
                                        <td id="order_{{$item->id}}" sum="{{$item->price_RUB}}">{{$item->price_RUB}}</td>
                                    @endif
                                    {{--<td><a class="content-table__add">Добавить в корзину</a></td>--}}
                                    <input type="hidden" value="{{$item->id}}" name="id">
                                    <input type="hidden" value="{{$item->price_RUB}}" name="price_RUB">
                                </tr>
                            @endforeach
                        @endforeach
                    @endif
                </table>
            </div>

            @include('components.pagination',['paginator' => $paginate_list])

        </div>
    </div>
    <div class="maintext blockbg">
        <div class='container'>
            <div class="maintext__title title_3">прайс-лист {{$cartridge_brand->title}}</div>
            <div class="maintext-block">
                @if(count($cartridge_brand->bottom_text_for_brands))
                    {!!$cartridge_brand->bottom_text_for_brands!!}
                @endif
            </div>
        </div>
    </div>
    <script>
            if($('.header-cart').length){
                var objToStick = $(".header-cart"); //Получаем нужный объект
                var topOfObjToStick = $(objToStick).offset().top; //Получаем начальное расположение нашего блока
                console.log(window.innerWidth);
                $(window).scroll(function () {
                    var windowScroll = $(window).scrollTop(); //Получаем величину, показывающую на сколько прокручено окно
                    if (windowScroll > topOfObjToStick+40) { // Если прокрутили больше, чем расстояние до блока, то приклеиваем его
                        $(objToStick).addClass("topWindow");
                        var right_margin = 260;
                        if(window.innerWidth < 1600){
                            right_margin = 0;
                        }
                        $('.topWindow').css("right", right_margin);
                    } else {
                        $(objToStick).removeClass("topWindow");
                    };
                });

            }
    </script>
@stop