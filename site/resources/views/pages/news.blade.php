@extends('layouts.default')
@section('content')
    <div class="content">
        <div class='container'>
            <div class="content-breadcrumbs">
                {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
            </div>
            <div class="content__title title_3">Новости</div>
            <div class="content-news">
                @if(count($news))
                    @foreach($news as $news_item)
                        <div class="mainnews-item">
                            <div class="mainnews-cell">
                                <div class="mainnews-image">
                                    <a href="{{route('newsitem',$news_item->_slug)}}"><img src="{{$news_item->image_url}}" alt="" /></a>
                                </div>
                            </div>
                            <div class="mainnews-cell">
                                <div class="mainnews-body">
                                    <a href="{{route('newsitem',$news_item->_slug)}}" class="mainnews-body__title">{{$news_item->title}}</a>
                                    <div class="mainnews-body__data">{{$news_item->date}}</div>
                                    <div class="mainnews-body__text">{{$news_item->preview}}<a class="mainnews-body__more" href="{{route('newsitem',$news_item->_slug)}}">Читать далее ›</a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            @include('components.pagination',['paginator' => $news])

        </div>
    </div>
@stop