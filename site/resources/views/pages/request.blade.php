@extends('layouts.default')
@section('content-leftside')
<aside class="content-leftside content-leftside__full">
    <div class="content-leftside-row row">
        @include('components.brands')
        <div class="content-body-centerside">
            <div class="content-body-breadcrumbs">
                {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
            </div>
                @if(!empty(Session::get('send')) && Session::get('send'))
                        <div class="content__title title_success">Ваша заявка № {{Session::get('code')}} от {{Session::get('date')}} успешно отправлена</div>
                @else
                        <div class="content__title">Оформить заявку</div>
                @endif
            <script>
                grecaptcha.ready(function() {
                    grecaptcha.execute("{{env('reCAPTCHA_KEY')}}", {action: "{{isset($rout) ? $rout : 'store_request'}}"}).then(function(token) {
                        var recaptchaResponse = document.getElementById('recaptchaResponse');
                        recaptchaResponse.value = token;
                    });;
                });
            </script>
            <div class="content-body-form">
                <div class="content-body-form__title">Подача заявок на продажу картриджей</div>
                {!! Form::open(
                        array(
                            'route' => 'store_request',
                            'method'=>'POST',
                            'novalidate' => 'novalidate',
                            'files' => true)) !!}
                        {!! csrf_field() !!}
{{--                <input type="hidden" name="recaptcha_response" id="recaptchaResponse">--}}
                    {{ HiddenCaptcha::render() }}
                    <div class="content-body-form-row row">
                        <div class="content-body-form-column">
                            <input data-value="Имя *" type="text" value="{{Input::old('name')}}" class="content-body-form__input" name="name" />
                            <input data-value="Телефон" type="text" value="{{Input::old('phone')}}" class="number_phone content-body-form__input @if($errors->first('phone')) err @endif" name="phone" />
                            <input data-value="E-mail *" type="text" value="{{Input::old('email')}}" class="content-body-form__input" name="email" />
                            <div class="select content-body-form__select">
                                <div class="select-title">
                                    <div class="select-title__arrow"></div>
                                    @if(Input::old('place'))
                                        <div class="select-title__value">{{Input::old('place')}}</div>
                                    @else
                                        <div class="select-title__value">Ваше месторасположение *</div>
                                    @endif
                                </div>
                                <div class="select-options">
                                    <div class="select-options-inside" style="overflow-y: hidden;" tabindex="5000">
                                        <div class="select-options__value">Екатеринбург</div>
                                        <div class="select-options__value">Свердловская область и Урал</div>
                                        <div class="select-options__value">Другой город</div>
                                    </div>
                                    <input type="hidden" name="place" value="{{Input::old('place')}}">
                                </div>
                            </div>
                            <div class="content-body-form__info">
                                <span>*</span> Поля обязательные для заполнения
                            </div>
                        </div>
                        <div class="content-body-form-column">
                            <div class="content-body-form-file row">
                                <div data-value="Фотографии (коробок и картриджи)" class="content-body-form-file__info">Фотографии (коробок и картриджи)</div>
                                <div class="content-body-form-file__btn btn">Обзор...</div>
                                <input class="content-body-form-file__file" type="file" name="images[]" multiple  />
                            </div>
                            <textarea name="mess" data-value="Что Вы хотите нам предложить"  class="content-body-form__input">{{Input::old('mess')}}</textarea>
                            {{--<div class="content-body-form-capcha row">--}}
                                {{--<div class="content-body-form-capcha-code">--}}
                                    {{--<a  class="content-body-form-capcha-code__refresh" id="refresh_captcha"></a>--}}
                                    {{--<div class="content-body-form-capcha-code__code"><img id="code" src="{{captcha_src('flat')}}" onclick="this.src ='/captcha/flat?'+Math.random()" alt="captcha"></div>--}}
                                {{--</div>--}}
                                {{--<div class="content-body-form-capcha-input">--}}
                                    {{--<input data-value="Введите код *" type="text" class="content-body-form__input @if($errors->first('code')) err @endif " name="code" />--}}
                                {{--</div>--}}
                                <input type="hidden" data-value="retain_request"  name="type"  />
                            {{--</div>--}}
                            <button class="content-body-form__btn content-body-form__btn_2 btn" type="submit" @if(!env('TEST_SITE', false)) onclick="yaCounter18363340.reachGoal('SENDFORMZAYAV')" @endif >Отправить заявку</button>
                            <div class="g-recaptcha" data-sitekey="your_site_key"></div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</aside>
@stop