@extends('layouts.default')
@section('content')
    <div class="content">
        <div class='container'>
            <div class="content-breadcrumbs">
                {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
            </div>
            <div class="content__title title_3">{!!$news->title!!}</div>
            <div class="content-news">
                <div class="content-news__image">
                    <a class="zoom" href="{{$news->big_img}}"><img src="{{$news->big_img}}" alt="{{$news->title}}" /></a>
                </div>
                <span class="mainnews-body__data">{{$news->date}}</span>
					<span class="mainnews-body__text">
						{!!$news->content!!}
					</span>
            </div>
        </div>
    </div>
@stop