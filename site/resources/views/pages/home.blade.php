@extends('layouts.default')

@section('content')

    @include('pages.home_components.mainsteps')

    @include('pages.home_components.mainformblock')

    @include('pages.home_components.mainruls_blockbg')

    @include('pages.home_components.mainpopular')

    @include('pages.home_components.mainnews_blockbg')

    @include('pages.home_components.mainbrands')

    @include('pages.home_components.maintext_blockbg')

@stop