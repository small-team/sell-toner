@extends('layouts.default')
@section('content-leftside')
<aside class="content-leftside content-leftside__full">
    <div class="content-leftside-row row">

     @include('components.brands')

        <div class="content-body-centerside">
            <div class="content-body-breadcrumbs">
                {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
            </div>
            <div class="content__title">Контактные данные</div>
            <div class="content-contact">
                <div class="content-contact-row row">
                    <div class="content-contact-column">
                    @if(isset($about_info['about_address']))
                        <p><strong>Адрес: </strong> {{ $about_info['about_address']}}</p>
                    @endif
                    @if(isset($about_info['about_timeblade']))
                        <p><strong>Время работы: </strong> {{ $about_info['about_timeblade']}}</p>
                    @endif
                    </div>
                    <div class="content-contact-column">
                    @if(isset($about_info['about_contact']))
                        <p><strong>Телефоны: </strong> {{ $about_info['about_contact']}}</p>
                    @endif
                    @if(isset($about_info['about_email']))
                        <p><strong>E-mail: </strong> <a href="mailto:manager@toner-sell.ru">{{ $about_info['about_email']}}</a></p>
                    @endif
                    </div>
                </div>
                <div class="content-contact__info">Предварительно обязательно созвониться с менеджером!</div>
                @if(isset($about_info['about_coordinates_l'])&& isset($about_info['about_coordinates_d']))
                <div id="map" class="content-contact__map" data-address="{{$about_info['about_address']}}" data-about_coordinates_l="{{$about_info['about_coordinates_l']}}" data-about_coordinates_d="{{$about_info['about_coordinates_d']}}"> </div>
                @else
                <div id="map" class="content-contact__map" data-address="{{$about_info['about_address']}}" data-about_coordinates_l="56.8683385" data-about_coordinates_d="60.5947636"> </div>
                @endif
            </div>
        </div>
    </div>
</aside>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
@stop