@extends('layouts.default')
@section('content-leftside')
<aside class="content-leftside content-leftside__full">
    <div class="content-leftside-row row">

     @include('components.brands')
     <div class="content-body-centerside">
        <div class="content-body-breadcrumbs">
            {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
        </div>
        <div class="content__title">@if(isset($info_page->title)){{$info_page->title}}@endif</div>
        <div class="content-article">
            @if(isset($info_page->content))
                {!!$info_page->content!!}
            @endif
        </div>
     </div>
     </div>
</aside>

@stop