@extends('layouts.default404')
@section('content-center')
<aside class="content-center content-center_noright">
    <div class="content__breadcrumbs">
        {!!\App\Http\Controllers\BreadcrumbsController::printBreadCrumbs()!!}
    </div>
    <div class="content__title">@if(isset($info_page->title)){{$info_page->title}}@endif</div>
    <div class="content-article">
        @if(isset($content_page_404))
        {!!$content_page_404!!}
        @endif
    </div>
</aside>
@stop
