var elixir = require('laravel-elixir');

var gulp = require('gulp');
var imagemin = require('gulp-imagemin');

gulp.task('imagemin', function () {

    gulp.src('resources/assets/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img'));

    gulp.src('resources/assets/images/blockbg/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/blockbg'));

    gulp.src('resources/assets/images/brands/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/brands'));

    gulp.src('resources/assets/images/icons/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/icons'));

    gulp.src('resources/assets/images/maininfoblock/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/maininfoblock'));

    gulp.src('resources/assets/images/products/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/products'));

    gulp.src('resources/assets/images/ruls/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/ruls'));

    gulp.src('resources/assets/images/steps/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/steps'));

    gulp.src('resources/assets/images/tittles/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/tittles'));
});

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */





elixir(function(mix) {

    mix.styles([
        'style.css',
        'jquery.autocompleter/jquery.autocompleter.css'
    ], 'public/assets/css')
        .scripts([
            'jquery.maskedinput.js',
            'jquery.autocompleter/jquery.autocompleter.js',
            'jquery.nicescroll.js',
            'script.js'
        ], 'public/assets/js')
        .version(['public/assets/css/all.css','public/assets/js/all.js']);

    mix.task('imagemin');
});